	<script src="../js/jquery.slim.js"></script> 
	<script src="../js/popper.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/scripts.js"></script>
	<script src="../js/menu-hover.js"></script>
	<script src="../js/validar-numero.js"></script> 
	<script src="libraries/DataTables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
	<script src="libraries/DataTables/DataTables-1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="libraries/DataTables/datatables.min.js"></script>
	<script src="libraries/DataTables/datatables.responsive.min.js"></script>
	<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
	<script src="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/js/bootstrap-datepicker.js"></script>
	<script src="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/js/locales/bootstrap-datepicker.es.js"></script>
	<script src="js/sa.min.js"></script>
	<script>
		$('.dataTable').DataTable({
			language: {
		        url: '//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json'
		    }
		});
	</script>