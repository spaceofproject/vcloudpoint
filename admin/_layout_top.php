
<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">   
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">       
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/css/datepicker3.css" rel="stylesheet"/>
<link rel="stylesheet" href="../css/styles.css">
<link rel="stylesheet" href="../css/footer.css">
<link rel="stylesheet" href="../css/container-inicio.css">
<link rel="stylesheet" href="../css/noticias.css">
<link rel="stylesheet" href="../css/caracteristicas.css">
<link rel="stylesheet" href="../css/divisor.css">
<link rel="stylesheet" href="../css/separador.css">
<link rel="stylesheet" href="../css/menu-activo.css">
<link rel="stylesheet" href="../css/galeria.css">
<link rel="stylesheet" href="../css/imagen-hover.css">
<link rel="stylesheet" href="../css/nav-tabs.css">
<link rel="stylesheet" href="../css/productos.css">
<link rel="stylesheet" href="libraries/DataTables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="libraries/DataTables/DataTables-1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="libraries/DataTables/datatables.min.css">
<link rel="stylesheet" href="css/sa.css">
<link rel="stylesheet" href="css/custom.css">
