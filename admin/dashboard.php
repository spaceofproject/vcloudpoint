<?php
 
	include "main.class.php";
	$main  = new main();
	$users = $main->getUsers();
	
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!----------------------  LAYOUT START ---------------------->
	<?php include "_layout_top.php" ?>
	<!----------------------  LAYOUT START ---------------------->
</head>

	<body>

		<!----------------------  MENU START ---------------------->
		<?php include "_menu.php" ?>
		<!----------------------  MENU END   ---------------------->
		<br><br>
		<section class = "content-header">
			<div class='container'>
			
				<div class='row'>
					<div class='col-md-12'>
						<?php if(isset($_SESSION['flash'])): ?>
							<?php if($_SESSION['flash']['status'] == 'success'): ?>
								<div class="alert alert-success" role="alert">
								  <?php echo $_SESSION['flash']['msg']; ?>
								</div>
							<?php endif; ?>
							<?php if($_SESSION['flash']['status'] == 'error'): ?>
								<div class="alert alert-danger" role="alert">
								  <?php echo $_SESSION['flash']['msg']; ?>
								</div>
							<?php endif; ?>
							
							<?php unset($_SESSION['flash']) ?>
						<?php endif; ?>
					</div>
				</div>

			
			
			
				<div class = "row">
					<div class="col-md-4">						
						<div class='card'>
							<div class='card-body'>
								<h5 class="card-title">Agregar Usuario</h5>
									
								<form method="post" action="main.controller.php"  autocomplete="false">
								
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												
												<label>Nombre</label>
												<input type = "text" name = "addUser[name]" class = "required form-control">
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												
												<label>Email</label>
												<input type = "text" name = "addUser[email]" class = "required email form-control">
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												
												<label>Contraseña</label>
												<input type = "password" name = "addUser[password]" class = "required form-control">
											</div>
										</div>
									</div>
									<div class="box-footer">
										<br>
										<button type="submit" id="submit" class="btn btn-primary pull-right">Guardar Datos</button>
									</div>
								</form>		
							</div>
						</div>			
					</div>
					
					<div class = "col-md-8">
						<div class='card'>
							<div class='card-body'>
									<h5 class="card-title">Listado de Usuarios</h5>
									<div class="table-responsive">
										<table class="table table-hover dataTable">
											<thead>
												<tr>
													<th>Id</th>
													<th>Nombre</th>
													<th>Email</th>
													<th></th>
												</tr>
											</thead>
											<tbody>																		
												<?php foreach($users as $row): ?>									
												<tr>
													<td><?php echo $row['user_id'] ?></td>
													<td><?php echo $row['name'] ?></td>
													<td><?php echo $row['email'] ?></td>										
													<td>
													<a type="button" class="btn btn-sm btn-icon btn-primary" data-toggle="modal" data-target="#modal<?php echo  $row['user_id'] ?>">
														  <i class = " fa fa-fw  fa-pencil" style='color:white'></i>
														</a>
														<a href = "javascript:void(0)" onclick='areYouSure("<?php echo md5($row['user_id']) ?>")' class="btn btn-sm btn-danger btn-icon ">
															<i class = " fa fa  fa-close"></i>
														</a>
													</td>
												</tr>
												<!-- Modal -->
												<div class="modal fade" id="modal<?php echo $row['user_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
														  <div class="modal-dialog" role="document">
														    <div class="modal-content">
														      <div class="modal-header">
														        <h5 class="modal-title" id="exampleModalLongTitle">Editar</h5>
														        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
														          <span aria-hidden="true">&times;</span>
														        </button>
														      </div>
														      
														      <form method="post" action="main.controller.php"  autocomplete="false" >
														      <div class="modal-body">
														        
														        	<input type='hidden' value='<?php echo $row['user_id'] ?>' name='editUser[userId]'>
														        	<div class="row">
																		<div class="col-md-12">
																			<div class="form-group">
																				
																				<label>Nombre:</label><br>
																				<input type = "text" name = "editUser[name]" class = "required form-control">
																			</div>
																		</div>
																		<div class="col-md-12">
																			<div class="form-group">																				
																				<label>Email:</label><br>
																				<input type = "text" name = "editUser[email]" class = "required email form-control">
																			</div>
																		</div>
																		<div class="col-md-12">
																			<div class="form-group">																				
																				<label>Password:</label><br>
																				<input type = "password" name = "editUser[password]" class = "required  form-control">
																			</div>
																		</div>
																	</div>	
														        													      
														      <div class="modal-footer">
														        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
														        <button type="submit" class="btn btn-primary">Actualizar</button>
														      </div>
														      </form>	
														    </div>
														  </div>
														</div>
												<?php endforeach; ?>
											</tbody>	
										</table>
									</div>
								</div>
							</div>	
						</div>
						
						
				
				</div>
			
		</section>
		
	   <!----------------------  LAYOUT END ---------------------->
	   <?php include "_layout_bottom.php" ?>
       <!----------------------  LAYOUT END ---------------------->
	

		<script>
		
			function areYouSure(id)
			{
				
				
				swal({
				  title: "¿Está seguro?",
				  text: "¿Está seguro que desea eliminar? Esta acción no se puede revertir",
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Sí, eliminar",
				  cancelButtonText: "Cancelar",
				  closeOnConfirm: false
				},
				function(){
				  	var url = "main.controller.php?ruser="+id;
				  	window.location.href = url;
				});
			}
		
		
		
		 
		</script>
	
	
	</body>
</html>