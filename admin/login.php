<?php 
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!----------------------  LAYOUT START ---------------------->
	<?php include "_layout_top.php" ?>
	<!----------------------  LAYOUT START ---------------------->
</head>

    <body>
          
	<div class='row'>
					<div class='col-md-12'>
						<?php if(isset($_SESSION['flash'])): ?>
							<?php if($_SESSION['flash']['status'] == 'success'): ?>
								<div class="alert alert-success" role="alert">
								  <?php echo $_SESSION['flash']['msg']; ?>
								</div>
							<?php endif; ?>
							<?php if($_SESSION['flash']['status'] == 'error'): ?>
								<div class="alert alert-danger" role="alert">
								  <?php echo $_SESSION['flash']['msg']; ?>
								</div>
							<?php endif; ?>
							
							<?php unset($_SESSION['flash']) ?>
						<?php endif; ?>
					</div>
				</div>
      
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content" style = "padding-top: 55px; padding-bottom: 60px;">
            <div class="container">
                <div class="col-md-4 ml-auto mr-auto">
                    

						<div class="card card-login card-plain">
						    <div class="card-header ">
							    <div class="logo-containerr text-center">
									<img src = "../img/logo vcloudpoint.png">
									
							    </div>
						    </div>
							<br>
							<form class="form" method="post" action="main.controller.php">
						    <div class="card-body ">        
						      <div class="input-group no-border form-control-lg">
						        <span class="input-group-prepend">
						          <div class="input-group-text">
						            <i class="fa fa fa-user"></i>
						          </div>
								</span>
								
						        <input type="text" class="form-control" name='login[email]' placeholder="Usuario" style='height:50px;'>
						      </div>
							  <br>
						      <div class="input-group no-border form-control-lg">
						        <div class="input-group-prepend">
						          <div class="input-group-text" >
						            <i class="fa fa fa-unlock"></i>
						          </div>
						        </div>
						        <input type="password" placeholder="Contraseña" name='login[password]' class="form-control" style='height:50px;'>
						      </div>
							</div>
							<div class="card-footer ">
					    	    <button type='submit' class="btn btn-primary btn-round btn-lg btn-block mb-3">Ingresar</button>
		                       
							
					   	  </div>
						</form> 
						</div>

                </div>
            
            
            
		
				
            
        </div>
        
        
	
	</div>


	   <!----------------------  LAYOUT END ---------------------->
	   <?php include "_layout_bottom.php" ?>
       <!----------------------  LAYOUT END ---------------------->
<script>
  $(document).ready(function(){
    demo.checkFullPageBackgroundImage();
  });
  
</script>

</body>
</html>
