<?php
class main {
	

	public function open_conn()
	{
		$mysqli = mysqli_connect("localhost","root","","vcloudpoint_db");		
		return $mysqli;
		
	}
	// login 
	public function login($email,$password)
	{
		$conn   = $this->open_conn();
		$sql = "SELECT user_id,name,email,password FROM user WHERE email = '$email' AND password = '$password'";
		$result=$conn->query($sql);
		$rows = $result->num_rows;
		
		if($rows > 0) {
			$row = $result->fetch_assoc();
			
			$_SESSION['email'] = $email;
			$_SESSION['password'] = $password;
			return true;
		}
		return false;
	}
	public function getUsers()
	{
		
		$conn   = $this->open_conn();
		$sql    = "SELECT user_id,name,email FROM user ";
		
		
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		
		$arr = array();	
		while($row = mysqli_fetch_assoc($result))
		{
			$arr[] = $row;
		}
		
		mysqli_close($conn);
		return $arr;
	}
	
	public function editUser($name,$email,$userId,$password)
	{
		
		$conn   = $this->open_conn();
		$sql    = "UPDATE user SET name='$name', email='$email', password='$password' WHERE user_id = '$userId'; ";
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		mysqli_close($conn);
		return true;
		    
	}
	
	
	public function removeUser($id)
	{
		
	    $conn   = $this->open_conn();
		$sql    = "delete from user where md5(user_id) = '$id' and user_id <> 25 limit 1";
		
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		mysqli_close($conn);
		return true;
        
	}
	
	
	public function addUser($name,$email,$password)
	{
		
		$conn   = $this->open_conn();
		$sql    = "insert into user (name,email,password) values ('$name','$email','$password'); ";
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		mysqli_close($conn);
		return true;
		    
	}
	
	// funcion para la categoria
	public function getCategories()
	{
		
		$conn   = $this->open_conn();
		$sql    = "SELECT study_case_category_id,name,img_path FROM study_case_category ";
		
		
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		
		$arr = array();	
		while($row = mysqli_fetch_assoc($result))
		{
			$arr[] = $row;
		}
		
		mysqli_close($conn);
		return $arr;
	}

	public function addCategory($name,$img_path)
	{
		
		$conn   = $this->open_conn();
		$sql    = "insert into study_case_category (name,img_path) values ('$name','$img_path'); ";
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		mysqli_close($conn);
		return true;
		    
	}
	
	
	public function editCategory($name,$img_path,$cid)
	{
		
		$conn   = $this->open_conn();
		$sql    = "UPDATE study_case_category SET name='$name', img_path='$img_path' WHERE study_case_category_id = '$cid'; ";
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		mysqli_close($conn);
		return true;
		    
	}

	public function removeCategory($id)
	{
		
	    $conn   = $this->open_conn();
		$sql    = "delete from study_case_category where md5(study_case_category_id) = '$id' ";
		
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		mysqli_close($conn);
		return true;
        
	}
	
	// funcion para casos de exito

	public function getCases()
	{
		
		$conn   = $this->open_conn();
		$sql    = "SELECT study_case_id,title,description FROM study_case ";
		
		
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		
		$arr = array();	
		while($row = mysqli_fetch_assoc($result))
		{
			$arr[] = $row;
		}
		
		mysqli_close($conn);
		return $arr;
	}
	
	
	
	
	public function removeCase($id)
	{
		
	    $conn   = $this->open_conn();
		$sql    = "delete from study_case where md5(study_case_id) = '$id'";
		
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		mysqli_close($conn);
		return true;
        
	}
	
	public function editCase($title,$description,$id)
	{
		
		$conn   = $this->open_conn();
		$sql    = "UPDATE study_case SET title='$title', description='$description' WHERE study_case_id = '$id'; ";
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		mysqli_close($conn);
		return true;
		    
	}

	
	public function addCase($title,$description)
	{
		
		$conn   = $this->open_conn();
		$sql    = "insert into study_case (title,description) values ('$title','$description'); ";
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		mysqli_close($conn);
		return true;
		    
	}
	
	public function getFile()
	{
		
		$conn   = $this->open_conn();
		$sql    = "SELECT study_case_image_id,img_path,study_case_id FROM study_case_image ";
		
		
		$result = mysqli_query($conn, $sql) or die(mysqli_error('Error!! en la base de datos'));
		
		$arr = array();	
		while($row = mysqli_fetch_assoc($result))
		{
			$arr[] = $row;
		}
		
		mysqli_close($conn);
		return $arr;
	}
}


?>