<?php

	include "main.class.php";
	$main = new main();

	session_start();
	 
	//LOGIN
	if(isset($_POST['login']))
	{
		$values    = $_POST['login'];
		$name      = $values['email'];
		$password  = $values['password'];
		
		if ($main->login($name,$password)){

			
			header('location: dashboard.php');	
		}else{

			$message = "Verifique sus credenciales";		
			$_SESSION['flash'] = array('status'=>'error','msg'=>$message);
		
        	header('location: login.php');
		}
       
	}
	
	// END LOGIN
	//PARA BORRAR AL USUARIO
	if(isset($_GET['ruser']))
	{

        $main->removeUser($_GET['ruser']);
		
		$message = "Se elimino con exito el registro";		
		$_SESSION['flash'] = array('status'=>'success','msg'=>$message);
		
        header('location: dashboard.php');						
		
	}



	//PARA AGREGAR AL USUARIO
	if(isset($_POST['addUser']))
	{
		$values = $_POST['addUser'];
		$name   = $values['name'];
		$email  = $values['email'];
		$pass   = $values['pass'];
		
		$main->addUser($name,$email,$pass);
		
		$message = "Se agrego con exito el registro";		
		$_SESSION['flash'] = array('status'=>'success','msg'=>$message);
		
        header('location: dashboard.php');	
	}

	// PARA EDITAR LOS USUARIO
	if(isset($_POST['editUser']))
	{
		$values = $_POST['editUser'];
		$name   = $values['name'];
		$email    = $values['email'];
		$password  = $values['password'];
		$userId = $values['userId'];

		$main->editUser($name,$email,$userId,$password);
		
		$message = "Se edito con exito el registro";		
		$_SESSION['flash'] = array('status'=>'success','msg'=>$message);
		
        header('location: dashboard.php');	
	}

	// PARA AGREGAR LA CATEGORIA 
	if(isset($_POST['addCategory']))
	{
		
		
		$values = $_POST['addCategory'];
		$name   = $values['name'];
		
		
		$dir = '../img/uploads/';
		$nombreArchivo=$_FILES['file']['name'];

		// Si no existe el directorio lo creas

		if (!file_exists($dir))
		{			
			mkdir($dir,0777);
		}

		// Ahora puedes mover la imagen al directorio

		if (move_uploaded_file($_FILES['file']['tmp_name'], $dir . $nombreArchivo)){
			$main->addCategory($name,$_FILES['file']['name']);
		
			$message = "Se agrego con exito el registro";		
			$_SESSION['flash'] = array('status'=>'success','msg'=>$message);
			
			header('location: category.php');	
		}else {
			$message = "Ocurrio un error!! verifique y vuelva a intentar";		
			$_SESSION['flash'] = array('status'=>'error','msg'=>$message);
			
			header('location: category.php');
		}
		
		
	}
	
	
	// PARA EDITAR LA CATEGORIA 
	if(isset($_POST['editCategory']))
	{
		$values = $_POST['editCategory'];
		$name   = $values['name'];
		$cid    = $values['category_id'];
		$file   = $values['name'];
		
		
		$dir = '../img/uploads/';
		$nombreArchivo=$_FILES['file']['name'];

		// Si no existe el directorio lo creas

		if (!file_exists($dir))
		{			
			mkdir($dir,0777);
		}

		// Ahora puedes mover la imagen al directorio

		if (move_uploaded_file($_FILES['file']['tmp_name'], $dir . $nombreArchivo)){
			$main->editCategory($name,$file,$cid);
		
			$message = "Se agrego con exito el registro";		
			$_SESSION['flash'] = array('status'=>'success','msg'=>$message);
			
			header('location: category.php');	
		}else {
			$message = "Ocurrio un error!! verifique y vuelva a intentar";		
			$_SESSION['flash'] = array('status'=>'error','msg'=>$message);
			
			header('location: category.php');
		}
	}

	//PARA BORRAR LA CATEGORIA
	if(isset($_GET['rcategory']))
	{

        $main->removeCategory($_GET['rcategory']);
		
		$message = "Se elimino con exito el registro";		
		$_SESSION['flash'] = array('status'=>'success','msg'=>$message);
		
        header('location: category.php');						
		
	}

	// PARA AGREGAR LOS CASOS DE EXITO 
	if(isset($_POST['addCase']))
	{
		$values = $_POST['addCase'];
		$title   = $values['title'];
		$description  = $values['description'];
		
		$main->addCase($title,$description);
		
		$message = "Se agrego con exito el registro";		
		$_SESSION['flash'] = array('status'=>'success','msg'=>$message);
		
        header('location: case.php');	
	}

	//PARA BORRAR LOS CASOS DE EXITO
	if(isset($_GET['rcase']))
	{

        $main->removeCase($_GET['rcase']);
		
		$message = "Se elimino con exito el registro";		
		$_SESSION['flash'] = array('status'=>'success','msg'=>$message);
		
        header('location: case.php');						
		
	}
	// PARA EDITAR LOS CASOS DE EXITO
	if(isset($_POST['editCase']))
	{
		$values = $_POST['editCase'];
		$title   = $values['title'];
		$id    = $values['case_id'];
		$description  = $values['description'];

		$main->editCase($title,$description,$id);
		
		$message = "Se edito con exito el registro";		
		$_SESSION['flash'] = array('status'=>'success','msg'=>$message);
		
        header('location: case.php');	
	}

	// PARA AGREGAR IMAGENES A LA GALERIA
	if(isset($_POST['addFile']))
	{
		$values = $_POST['addFile'];
		
		$dir = '../img/uploads/';
		$nombreArchivo=$_FILES['file']['name'];

		// Si no existe el directorio lo creas

		if (!file_exists($dir))
		{			
			mkdir($dir,0777);
		}

		// Ahora puedes mover la imagen al directorio

		if (move_uploaded_file($_FILES['file']['tmp_name'], $dir . $nombreArchivo)){
			$main->addFile($_FILES['file']['name']);
		
			$message = "Se agrego con exito el registro";		
			$_SESSION['flash'] = array('status'=>'success','msg'=>$message);
			
			header('location: addFile.php');	
		}else {
			$message = "Ocurrio un error!! verifique y vuelva a intentar";		
			$_SESSION['flash'] = array('status'=>'error','msg'=>$message);
			
			header('location: addFile.php');
		}
		
		
	}
	
?>