<?php
 session_start();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">   
            <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
            <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">       
            <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
            <link href="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/css/datepicker3.css" rel="stylesheet"/>
			<link rel="stylesheet" href="css/styles.css">
			<link rel="stylesheet" href="css/menu.css">
			<link rel="stylesheet" href="css/footer.css">
			<link rel="stylesheet" href="css/container-inicio.css">
			<link rel="stylesheet" href="css/noticias.css">
			<link rel="stylesheet" href="css/caracteristicas.css">
			<link rel="stylesheet" href="css/divisor.css">
			<link rel="stylesheet" href="css/separador.css">
			<link rel="stylesheet" href="css/menu-activo.css">
			<link rel="stylesheet" href="css/formulario.css">
			<link rel="stylesheet" href="css/galeria.css">
			<link rel="stylesheet" href="css/imagen-hover.css">
			<link rel="stylesheet" href="css/nav-tabs.css">
			<link rel="stylesheet" href="css/productos.css">
			<link rel="stylesheet" href="css/now-ui-dashboard.css">
			<link rel="stylesheet" href="DataTables/DataTables-1.10.16/css/dataTables.bootstrap.min.css">
			<link rel="stylesheet" href="DataTables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
			<link rel="stylesheet" href="DataTables/DataTables-1.10.16/css/dataTables.foundation.min.css">
			<link rel="stylesheet" href="DataTables/DataTables-1.10.16/css/dataTables.jqueryui.min.css">
			<link rel="stylesheet" href="DataTables/DataTables-1.10.16/css/dataTables.semanticui.min.css">
            <link rel="stylesheet" href="DataTables/DataTables-1.10.16/css/jquery.dataTables.min.css">
            <link rel="stylesheet" href="DataTables/buttons-1.5.1/css/buttons.bootstrap.min.css">
            <link rel="stylesheet" href="DataTables/buttons-1.5.1/css/buttons.bootstrap4.min.css">
            <link rel="stylesheet" href="DataTables/buttons-1.5.1/css/buttons.dataTables.min.css">
            <link rel="stylesheet" href="DataTables/buttons-1.5.1/css/buttons.foundation.min.css">
            <link rel="stylesheet" href="DataTables/buttons-1.5.1/css/buttons.jqueryui.min.css">
            <link rel="stylesheet" href="DataTables/buttons-1.5.1/css/buttons.semanticui.min.css">
            <link rel="stylesheet" href="DataTables/buttons-1.5.1/css/common.scss">
            <link rel="stylesheet" href="DataTables/buttons-1.5.1/css/mixins.scss">
            <link rel="stylesheet" href="DataTables/datatables.min.css">
		</head>
    <body>
		<menu class = "card-2" style = "background: #0652DD;margin-top: 0px;padding-top: 15px;">
			<div class="box-body">
				<div class="row">
					<a href="dashboard.php">
					<div class = "card-2" style = "background: #12CBC4; border-radius: 30px;">
						<div class="form-group">
							<h4 style = "margin-left: 45px;margin-right: 45px;padding-top: 10px;margin-top: 0px;margin-bottom: 10px;">
								Usuarios
							</h4>
						</div>
					</div>
					</a>
                    &nbsp;&nbsp;&nbsp;
                    <a href="category.php">
						<div class = "card-2" style = "background: #12CBC4; border-radius: 30px;">
							<div class="form-group">
								<h4 style = "margin-left: 40px;margin-right: 40px;padding-top: 10px;margin-top: 0px;margin-bottom: 10px;">
									Categorías
								</h4>
							</div>
                        </div>
                    </a>
					&nbsp;&nbsp;&nbsp;
							<div class = "card-2" style = "background: #12CBC4; border-radius: 30px;">
								<div class="form-group">
									<h4 style = "margin-left: 30px;margin-right: 30px;padding-top: 10px;margin-top: 0px;margin-bottom: 10px;">
										Casos de éxito
									</h4>
								</div>
							</div>			
				</div>
			</div>					
		</menu>
		<br><br>
		<section class = "content-header">
			<div class = "col-md-12">
				<div class = "row">
					<div class="col-md-6">
						<div class = "card col-md-12" style="border-radius: 5.25rem !important;" id = "usuario">
						<!-- <div class = "card" style = "border_radius:30px; background: #009432;">< ?php echo $_REQUEST['msj']; ?></div> -->
					
							<div>
								<h3 class="box-title">Agregar Casos de Éxito</h3>
							</div>	
								<form method="post" action="casosForm.php"  autocomplete="false" enctype="multipart/form-data">
								
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<br>
												<label>Titulo</label>
												<input type = "text" name = "title" class = "required form-control">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<br>
												<label>Descripción</label>
												<textarea class = "form-control required" name = "description"></textarea>
											</div>
										</div>
										
									</div>
									<div class="box-footer">
										<button type="reset" onclick="window.location='#'" class="btn btn-danger"  style = "border-radius: 5.25rem !important;	">Cancelar</button>
										<button type="submit" id="submit" class="btn btn-primary pull-right"  style = "border-radius: 5.25rem !important;	">Guardar Datos</button>
									</div>
								</form>
						</div>
					</div>
					<?php
					$mysqli=new mysqli("localhost","root","","vcloudpoint_db"); 
	
					if(mysqli_connect_errno()){
						echo 'Conexion Fallida : ', mysqli_connect_error();
						exit();
					}
					$sql = "SELECT study_case_id,title,description FROM study_case ";
					$result=$mysqli->query($sql);
					$rows = $result->num_rows;
					
					if($rows > 0) {
					
					?>
                    <div class = "col-md-6 card" style = "border-radius: 5.25rem !important;">
                    <?php if(isset($_SESSION['flash'])): ?>
                       <?php echo $_SESSION['flash']['msg'] ?>
                       <?php endif; ?>
						<div class="box-body table-responsive">
							<table class="table table-hover dataTable">
								<thead>
									<tr>
										<th>Id</th>
										<th>Titulo</th>
										<th>Descripción</th>
										<th>Herramientas</th>
									</tr>
								</thead>
								<tbody>
									
                                    <?php while($row = $result->fetch_assoc())
                                    
									{
                                        ?>
									<tr>
									<?php
										echo "<td>"; echo $row['study_case_id']; echo "</td>";
										echo "<td>"; echo $row['title']; echo "</td>";
										echo "<td>"; echo $row['description']; echo "</td>";
									   
									?>
									<td>
										<a href = "casosDelete.php?id=<?php echo $row['study_case_id'];?>" class="btn btn-sm btn-danger btn-icon "><i class = " fa fa  fa-close"></i></a>
									</td>
									</tr>
								<?php } }?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		

	
    
	</body>
</html>
<script src="js/jquery.slim.js"></script>
<script src="js/app.js"></script>
<script src="js/menu-activo.js"></script>
<script src="js/popper.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/scripts.js"></script>
<script src="js/menu-hover.js"></script>
<script src="js/validar-numero.js"></script>
<script src="DataTables/DataTables-1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="DataTables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="DataTables/DataTables-1.10.16/js/dataTables.foundation.min.js"></script>
<script src="DataTables/DataTables-1.10.16/js/dataTables.jqueryui.min.js"></script>
<script src="DataTables/DataTables-1.10.16/js/dataTables.semanticui.min.js"></script>
<script src="DataTablesDataTables-1.10.16/js/jquery.dataTables.min.js"></script>
<script src="DataTables/buttons-1.5.1/js/buttons.bootstrap.min.js"></script>
<script src="DataTables/buttons-1.5.1/js/buttons.bootstrap4.min.js"></script>
<script src="DataTables/buttons-1.5.1/js/buttons.colVis.min.js"></script>
<script src="DataTables/buttons-1.5.1/js/buttons.foundation.min.js"></script>
<script src="DataTables/buttons-1.5.1/js/buttons.html5.min.js"></script>
<script src="DataTables/buttons-1.5.1/js/buttons.jqueryui.min.js"></script>
<script src="DataTables/buttons-1.5.1/js/buttons.print.min.js"></script>
<script src="DataTables/buttons-1.5.1/js/buttons.semanticui.min.js"></script>
<script src="DataTables/datatables.min.js"></script>
<script src="DataTables/datatables.responsive.min.js"></script>
<script src="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdn.jsdelivr.net/bootstrap.datepicker-fork/1.3.0/js/locales/bootstrap-datepicker.es.js"></script>

<script>
  $(document).ready(function(){
    demo.checkFullPageBackgroundImage();

  });
		$(function() {
		   $('#user_userRole').change(function () {
				if ($(this).val() == 4) {
	
				}
		   });
		
		$('.dataTable').DataTable({
			language: {
		        url: '//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json'
		    }
		});
	});
	</script>