<!DOCTYPE html>
<html lang="en">
	<head>
		<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
			<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|PT+Sans:400,700|Roboto:400,700,900" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
			<link rel="stylesheet" href="css/styles.css">
			<link rel="stylesheet" href="css/menu.css">
			<link rel="stylesheet" href="css/footer.css">
			<link rel="stylesheet" href="css/container-inicio.css">
			<link rel="stylesheet" href="css/noticias.css">
			<link rel="stylesheet" href="css/caracteristicas.css">
			<link rel="stylesheet" href="css/divisor.css">
			<link rel="stylesheet" href="css/separador.css">
			<link rel="stylesheet" href="css/menu-activo.css">
			<link rel="stylesheet" href="css/formulario.css">
			<link rel="stylesheet" href="css/galeria.css">
			<link rel="stylesheet" href="css/imagen-hover.css">
			<link rel="stylesheet" href="css/nav-tabs.css">
			<link rel="stylesheet" href="css/productos.css">

			<link rel="icon" href="img/logo.ico">
	</head>

	<body onselectstart="return false">
		<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="height:auto;">
			<div class="container">
				<div class="col-md-3">
					<a class="navbar-brand" href="index.php">
						<img src="img/logo vcloudpoint.png" style="margin:auto; width:200px; height:auto; padding:10px;" alt=""> 
					</a>

					<button style="border:none;  background-color:#0c96cc;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span style="background-color:#0c96cc;" class="navbar-toggler-icon">
						</span>
					</button>
				</div>

				<div class="col-md-9" style="margin-left:30px;">
					<div  class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active" style="margin-right:5px;">
								<a class="nav-link home" style="padding-right:0px; padding-left:0px;" href="index.php">
									<i class="fas fa-home"></i>
									<span class="sr-only">(current)</span>
								</a>
							</li>

							<li class="nav-item" style="margin-right:5px;">
								<a class="nav-link productos" style="padding-right:0px; padding-left:0px;" href="productos.php">
									<i class="fas fa-stop"></i>
								</a>
							</li>

							<li id="m-industria" style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#industria" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-building"></i>
								</a>
								<ul class="dropdown-menu" id="industria">
									<li>
										<a href="educacion.php">
											<i class="fa fa-graduation-cap"></i>
										</a>
									</li>

									<li>
										<a href="pymes.php">
											<i class="fas fa-briefcase"></i>
										</a>
									</li>

									<li>
										<a href="servicios.php">
											<i class="fas fa-comments"></i>
										</a>
									</li>

									<li>
										<a href="espacios-publicos.php">
											<i class="fas fa-cloud"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#soporte" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" 			aria-expanded="true">
									<i class="fas fa-user-shield"></i>
								</a>
								<ul class="dropdown-menu" id="soporte">
									<li>
										<a href="instalacion.php">
											<i class="fas fa-download"></i>
										</a>
									</li>
									<li>
										<a href="preguntas-frecuentes.php">
											<i class="far fa-question-circle"></i>
										</a>
									</li>
									<li>
										<a href="complementos.php">
											<i class="fas fa-plus"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" 
								class="dropdown">
								<a href="#" data-target="#compania" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-calendar-alt"></i>
								</a>
								<ul class="dropdown-menu" id="compania">
									<li>
										<a href="nosotros.php">
											<i class="fa fa-industry"></i>
										</a>
									</li>
									<li>
										<a href="partner.php">
											<i class="fas fa-user-tie"></i>
										</a>
									</li>
									<li>
										<a href="noticias.php">
											<i class="far fa-newspaper"></i>
										</a>
									</li>
									<li>
										<a href="contacto.php">
											<i class="fa fa-phone-square"></i>
										</a>
									</li>
								</ul>
							</li>

							<li class="" style="padding-left:10px;">
								<a id="menu-casos" style="color:#666666 ; padding-right:0px; padding-left:0px;  font-size:13px; margin-top:8px;" class="nav-link casos-exito" href="casos_exito.php">
									Casos de éxito
								</a>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

  
	
	<div class="row" style="background-color:#0b96ce; height:120px; margin-top:90px;">
		<div class="container">
			<p style="font-size:40px; color:white; margin-top:30px;">Guía de instalación y descargas</p>
		</div>
	</div>

	<div class="container" style="margin-bottom:10px;">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb" style="background-color:transparent;">
					<li style="margin-top:-2px!important;">
						<a style="font-size:12px; color:#8b8b8b; " href="index.php">
							<img style="width:15px; margin-top:-6px;" src="img/glyphicons/png/home.png" alt="">  Inicio 
						</a>
					</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Soporte</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Descargas</li>
				</ol>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-12 col-md-2  pt-4">
				<a href="instalacion.php" class="boton-productos btn  btn-outline-danger">
					Instalación
				</a>
			</div>
            <div class="col-12 col-md-2  pt-4">
                <a href="descargas.php" class="boton-productos btn  btn-outline-danger activo">
                    Descargas
                </a>
            </div>
            <div class="col-12 col-md-2  pt-4">
                <a href="documentacion.php" class="boton-productos btn  btn-outline-danger">
                    Documentación
                </a>
            </div>
            <div class="col-md-3">

            </div>
        </div>
	</div>

<div class="container pb-3">
  <h2 style="color:#0c96cc;">Software vMatrix 2.3.6</h2>
  <div class="row">
    <div class="col-12 col-md-3">
      <img src="img/vmatrix.png" style="width:100px; margin-bottom:30px; margin-right:auto; margin-left:auto; display:flex;">
    </div>
    <div class="col-12 col-md-3">
        <p class="text-justify" style="font-size:12px; color:black;">El software vMatrix Server es el componente central del sistema vCloudPoint, se ejecuta en los servidores compartidos o de gestión.
          <br>
          <br>
          Contiene el administrador de conexión que ofrece múltiples sesiones de escritorio para los usuarios de terminales y
          una completa serie de herramientas de gestión para el administrador que permiten administrar todo el sistema vCloudPoint.
        </p>
    </div>
    <div class="col-12 col-md-3 pb-5">
		<p style="margin-bottom:10px; text-align: justify; font-size:11px; font-weight: bold; color:black;" id="preguntas" class="preguntas" data-toggle="collapse" data-target="#version236" aria-expanded="true"> 
			Version 2.3.6 (19 de Marzo de 2019) - Nuevo</p>
			<div class="collapse" id="version236">
				<p style="font-size:12px; color:black;">
					La versión 2.3.6 de vMatrix Server Manager incluye correcciones importantes para mejorar la compatibilidad general y la confiabilidad de todo el sistema informático. <br><br>
					<b>Cambios</b><br><br>
					- Se agrego el "Modo Mejorado" en la pestaña "configuración" para mejorar la compatibilidad de la aplicación.<br><br>
					- Se agregó la ventana emergente para solicitar que se reinicie el sistema después de deshabilitar la virtualización de IP.<br><br>
					- Se agregó la función de conexión automática de terminal después de que la sesión de escritorio se desconecte o cierre sesión en escenarios desatendidos. <br><br>
					- Los administradores pueden cargar y bloquear fondos de pantalla personalizados para terminales en la administración del servidor. <br><br>
					- Se agregó soporte multilenguaje para el registro de administración. <br><br>
					- Se agregó la configuración de DNS para las terminales. <br><br>
					- Se ha agregado el icono de la consola de administración en el escritorio. <br><br>
					- Se agregó una ventana emergente para mostrar iconos ocultos en la barra de tareas. <br><br>
					- Se añadio el idioma ucraniano. <br><br>
					<b>Solucionado </b> <br><br>
					- Se solucionó el problema de que al copiar un archivo grande en un disco U conectado a la terminal se podían bloquear las sesiones de escritorio. <br><br>
					- Se solucionó el problema de que los usuarios no pueden mostrarse cuando el controlador de dominio actuando también como host compartido. <br><br>
					- Se solucionó el error que al habilitar la virtualización de IP puede causar un bloqueo aleatorio de "DaemonService" y una falla de conexión del usuario. <br><br>
					- Se solucionado el problema que el tiempo de inicio de sesión del usuario no se muestra en la pestaña "Administración de usuarios". <br><br>
					- Se solucionó el posible error .Net al cerrar la sesión de un usuario. <br><br>
					- Se solucionó el problema de cpaccel.exe en el host cuando el administrador inicia sesión justo después de cerrar la sesión. <br><br>
					- Se solucionó el problema de la virtualización de IP que el dispositivo NIC seleccionado no se pudo guardar en algunos casos, lo que causó que fallara la configuración de la IP virtual estática. <br><br>
					- Se solucionó el problema de que el nombre de la terminal no se ha conectado al host. <br><br>
					- Se solucionó el problema del error de inicio de sesión después de que un usuario cierra la sesión, lo que indica que no puede iniciar sesión repetidamente. <br><br>
					- Se solucionó el error de excepción de memoria después de cerrar la sesión de los usuarios en los sistemas Win7 / 08R2. <br><br>
					... <br><br>
					<b>Mejorado </b> <br><br>
					- Se mejoró la secuencia de asignación de IP virtual modificada en el inicio de sesión del usuario para reducir posibles conflictos de direcciones IP virtuales. <br><br>
					- Se mejoró la lógica en la conexión de la terminal optimizada en VPN para solucionar el problema de que las terminales no pueden iniciar sesión cuando se utiliza algún software VPN. <br><br>
					- Se modificó el diseño interno del menú "Configuración de red" dentro del menú del botón derecho de la pestaña "Administración de dispositivos" para permitir una configuración unificada de las direcciones IP de múltiples terminales. <br><br>
					- Se eliminó el menú separado de "Dirección IP fija" dentro del menú del botón derecho de la pestaña "Administración de dispositivos" para mover la configuración de la dirección IP fija al menú "Configuración de red".<br><br>
					... <br><br>
					
					
				</p>
			</div>
		<br><br>
		
		<p style="margin-bottom:10px; text-align: justify; font-size:11px; font-weight: bold; color:black;" id="preguntas" class="preguntas" data-toggle="collapse" data-target="#version231" aria-expanded="true"> 
			Version 2.3.1 (16 de agosto de 2018)</p>
			<div class="collapse" id="version231">
				<p style="font-size:12px; color:black;">
					<b>Corregido:</b> se corrigió el error de que los usuarios no pueden iniciar sesión con la cuenta de dominio en algunas circunstancias.<br><br>
					<b>Corregido:</b> se corrigió el error de ralentización aleatoria al reproducir video a través del reproductor VLC.<br><br>
					<b>Corregido:</b> el sistema de audio ahora funciona correctamente en Windows 10 1803.
				</p>
			</div>
		<br><br>
		
        <p style="margin-bottom:10px; text-align: justify;font-size:11px; color:black;" id="preguntas" class="preguntas" data-toggle="collapse" data-target="#uno" aria-expanded="true"> 
			Version 2.3.0 (26 de julio de 2018)</a></p>
            <div class="collapse" id="uno">
                <p style="font-size:12px; color:black;">
                <b>Nuevo:</b> configuración por lotes permitida de direcciones IP virtuales estáticas. <br><br>

                <b>Nuevo:</b> filtrado y búsqueda de usuarios agregados, y visualización del número de usuario en línea en la página de administración de usuarios. <br><br>

                <b>Nuevo:</b> menú agregado de desconexión y desconexión en la bandeja de tareas de las sesiones de la terminal. <br><br>

                <b>Nuevo:</b> mensaje de burbuja agregado cuando un dispositivo terminal establece la conexión. <br><br>

                <b>Nuevo:</b> soporte adicional para las versiones de VLC Player 3.0.x. <br><br>

                <b>Nuevo:</b> soporte adicional para la redirección de dispositivos en serie. <br><br>

                <b>Nuevo:</b> agregado idioma turco.<br><br>

                <b>Mejorado:</b> compatibilidad de virtualización IP mejorada.<br><br>

                <b>Mejorado:</b> compatibilidad mejorada con la redirección de dispositivos USB.<br><br>

                <b>Mejorado:</b> rendimiento optimizado de la operación de la interfaz de gestión.<br><br>

                <b>Mejorado:</b> se cambiaron las pantallas del ícono de la bandeja de tareas y las imágenes del perfil del usuario para vChat.<br><br>

                <b>Mejorado:</b> oculte el icono USB de la bandeja de tareas para las sesiones de usuario que se ejecutan en dispositivos de redirección USB no compatibles.<br><br>

                <b>Corregido:</b> se corrigió el error de que los administradores pueden tomar el control de las sesiones de usuario a través de las operaciones de teclado y mouse, incluso cuando no están autorizadas para ver las sesiones de los usuarios.<br><br>

                <b>Corregido:</b> se corrigió el error de que cuando el software de gestión se ve obligado a reiniciarse, no se puede identificar el estado del usuario.<br><br>

                <b>Corregido:</b> solucionado el error que los servicios de inicio de sesión pueden congelarse después de largas horas de operación.<br><br>

                <b>Corregido:</b> se solucionó el error que los textos de la interfaz de administración dejan de responder después de frecuentes cambios de idioma.<br><br>

                <b>Corregido:</b> se solucionó el error de que algunos elementos en las páginas de administración de usuarios, información de usuario, registros de comportamiento, registros de administración no responden a tiempo para el cambio de idioma.<br><br>

                <b>Corregido:</b> se solucionó el error que indica que las solicitudes de error para la asignación de usuarios locales no se muestran en el idioma correcto.<br><br>

                <b>Corregido:</b> solucionó el error que las sesiones pueden congelar durante la iniciación si el fondo de escritorio está habilitado.<br><br>

                <b>Corregido:</b> solucionado ese error que la letra de la unidad de la partición privada de los terminales y el dispositivo de almacenamiento USB no puede mostrar correctamente.<br><br>

                <b>Corregido:</b> se corrigió el error que el contenido de la página de algunas aplicaciones como Firefox, puede no mostrarse correctamente en los clientes.<br><br>

                <b>Corregido:</b> solucionó el problema de que el modo de transmisión no funcionaba correctamente en los clientes que reproducían video.<br><br>

                <b>Corregido:</b> se solucionó el error que los usuarios aún pueden iniciar sesión en el host cuando se activa el modo de mantenimiento.<br><br>

                <b>Corregido:</b> solucionó el problema de que los usuarios de los clientes no pueden iniciar sesión en un servidor de Windows Server 2003.<br><br>

                <b>Corregido:</b> se solucionó el error que algunos controladores pueden perder después de actualizar el sistema Windows 10 o Windows Server 2016.<br><br>
                </p>
            </div>
		<br><br>
    </div>
    <div class="col-12 col-md-3 text-center">
      <a class="boton-productos btn  btn-outline-danger" href="descargas/VMATRIX_2.3.6.rar">Descargar</a>
    </div>

  </div>
</div>







<div class="row" style="background-color:#333333;">
	<div class="container">
		<div class="row" style="margin-right:15px; margin-left:15px;">
			<div class="col-md-3">
				<img class="img-fluid  pt-3 pb-3" src="img/logo vcloudpoint1.png ">
				<p class="text-justify pt-2 footer-descripcion">
					Las tecnologías vCloudPoint hacen que la informática de escritorio sea extremadamente simple y económica. Debido a la excelente
					flexibilidad y simplicidad de sus cliente cero, puede implementar, usar y administrar fácilmente escritorios para docenas
					a cientos de usuarios a una fracción de los costos.
				</p>
			</div>
		
			<div class="col-md-3">
				<p style="color:white;  font-size:20px;" class="pt-3 pb-3 text-center">Contacto</p>
				<a>
					<p class="text-center" style="font-size:13px;"><i class="far fa-envelope"></i>  ventas@vcloudpoint.com.mx</p>
				</a>
				<hr style="background-color:white;">
				<p class="text-center" style="font-size:14px;"><i class="fas fa-phone-volume"></i> 01 (55) 5615-2916</p>
			</div>

			<div class="col-md-3">
				<p style="color:white; font-family: sans-serif; font-size:20px;" class="pt-3 pb-3 text-center">Noticias recientes</p>
				<a href="18-abril.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>
						Aviso: Se solucionó el problema con la actualización del sistema de
						Windows que causaba el inicio de sesión de varios usuarios no funcionara.
					</p>
				</a>
				<p style="font-size:10px;">18 de Abril del 2019</p>
				<hr class="footer2">
				<a href="18-marzo.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>VCLOUDPOINT China fue otorgado como miembro de CEEIA y SZEEIA.</p>
				</a>
				<p style="font-size:10px;">18 de Marzo del 2019</p>
			</div>

			<div class="col-md-3">
				<br>
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:40px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="http://www.facebook.com/vCloudPointMX/"> <i class="fab fa-facebook-f"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://youtu.be/cmTRDUURWTc"><i class="fab fa-youtube"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.linkedin.com/in/vcloudpoint-mexico-86a194168/"><i class="fab fa-linkedin-in"></i></a>
					</div>
				</div>
				
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:50px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://twitter.com/vcloudmx?lang=es"><i class="fab fa-twitter"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.instagram.com/vcloudpointmx/"><i class="fab fa-instagram"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--barra-progreso-->
<script type="text/javascript">
	function animar (){
		document.getElementById("barra1").classList.toggle ("final1");
		document.getElementById("barra2").classList.toggle ("final2");
		document.getElementById("barra3").classList.toggle ("final3");

	}
	window.onload = function (){
	  animar();

	}
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?51BGgA3dFcKcI1mPpd4Un7VFblV9TgM1";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


<footer style="background-color:#262626; height: auto;">
	<div class="container">
		<div class="row">
			<p style="color:#c5c5c5; font-size:14px;" class="text-center copyright w-100">vCloudPoint 2019. Todos los derechos reservados</p>
		</div> <!--.row-->
	</div><!--.container-->
</footer>









    <script src="js/jquery.slim.js"></script>
	<script src="js/app.js"></script>
    <script src="js/menu-activo.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/menu-hover.js"></script>
    <script src="js/validar-numero.js"></script>
  </body>
</html>
