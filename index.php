
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
			<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|PT+Sans:400,700|Roboto:400,700,900" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
			<link rel="stylesheet" href="css/styles.css">
			<link rel="stylesheet" href="css/menu.css">
			<link rel="stylesheet" href="css/footer.css">
			<link rel="stylesheet" href="css/container-inicio.css">
			<link rel="stylesheet" href="css/noticias.css">
			<link rel="stylesheet" href="css/caracteristicas.css">
			<link rel="stylesheet" href="css/divisor.css">
			<link rel="stylesheet" href="css/separador.css">
			<link rel="stylesheet" href="css/menu-activo.css">
			<link rel="stylesheet" href="css/formulario.css">
			<link rel="stylesheet" href="css/galeria.css">
			<link rel="stylesheet" href="css/imagen-hover.css">
			<link rel="stylesheet" href="css/nav-tabs.css">
			<link rel="stylesheet" href="css/productos.css">

			<link rel="icon" href="img/logo.ico">
	</head>

	<body onselectstart="return false">
		<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="height:auto;">
			<div class="container">
				<div class="col-md-3">
					<a class="navbar-brand" href="index.php">
						<img src="img/logo vcloudpoint.png" style="margin:auto; width:200px; height:auto; padding:10px;" alt=""> 
					</a>

					<button style="border:none;  background-color:#0c96cc;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span style="background-color:#0c96cc;" class="navbar-toggler-icon">
						</span>
					</button>
				</div>

				<div class="col-md-9" style="margin-left:30px;">
					<div  class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active" style="margin-right:5px;">
								<a class="nav-link home" style="padding-right:0px; padding-left:0px;" href="index.php">
									<i class="fas fa-home"></i>
									<span class="sr-only">(current)</span>
								</a>
							</li>

							<li class="nav-item" style="margin-right:5px;">
								<a class="nav-link productos" style="padding-right:0px; padding-left:0px;" href="productos.php">
									<i class="fas fa-stop"></i>
								</a>
							</li>

							<li id="m-industria" style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#industria" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-building"></i>
								</a>
								<ul class="dropdown-menu" id="industria">
									<li>
										<a href="educacion.php">
											<i class="fa fa-graduation-cap"></i>
										</a>
									</li>

									<li>
										<a href="pymes.php">
											<i class="fas fa-briefcase"></i>
										</a>
									</li>

									<li>
										<a href="servicios.php">
											<i class="fas fa-comments"></i>
										</a>
									</li>

									<li>
										<a href="espacios-publicos.php">
											<i class="fas fa-cloud"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#soporte" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" 			aria-expanded="true">
									<i class="fas fa-user-shield"></i>
								</a>
								<ul class="dropdown-menu" id="soporte">
									<li>
										<a href="instalacion.php">
											<i class="fas fa-download"></i>
										</a>
									</li>
									<li>
										<a href="preguntas-frecuentes.php">
											<i class="far fa-question-circle"></i>
										</a>
									</li>
									<li>
										<a href="complementos.php">
											<i class="fas fa-plus"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" 
								class="dropdown">
								<a href="#" data-target="#compania" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-calendar-alt"></i>
								</a>
								<ul class="dropdown-menu" id="compania">
									<li>
										<a href="nosotros.php">
											<i class="fa fa-industry"></i>
										</a>
									</li>
									<li>
										<a href="partner.php">
											<i class="fas fa-user-tie"></i>
										</a>
									</li>
									<li>
										<a href="noticias.php">
											<i class="far fa-newspaper"></i>
										</a>
									</li>
									<li>
										<a href="contacto.php">
											<i class="fa fa-phone-square"></i>
										</a>
									</li>
								</ul>
							</li>

							<li class="" style="padding-left:10px;">
								<a id="menu-casos" style="color:#666666 ; padding-right:0px; padding-left:0px;  font-size:13px; margin-top:8px;" class="nav-link casos-exito" href="casos_exito.php">
									Casos de éxito
								</a>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

  

<div class="container-fluid" style="margin-top:80px; border-bottom:1px solid #ccc; padding-left: 0px; padding-right: 0px;">
	<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
  		<div class="carousel-inner">
    		<div class="carousel-item active">
      			<img src="img/carousel/1.png" class="d-block w-100" alt="...">
    		</div>

    		<div class="carousel-item">
    			<a href="https://vcloudpoint.com.mx/beneficios.php">
	      			<img src="img/carousel/2.png" class="d-block w-100" alt="...">
	    		</a>
	    	</div>

	    	<div class="carousel-item">
    			<a href="https://vcloudpoint.com.mx/beneficios.php">
      				<img src="img/carousel/3.png" class="d-block w-100" alt="...">
    			</a>
    		</div>

    		<div class="carousel-item">
    			<a href="https://vcloudpoint.com.mx/casos_exito.php">
      				<img src="img/carousel/4.png" class="d-block w-100" alt="...">
    			</a>
    		</div>

    		<div class="carousel-item">
      			<img src="img/carousel/5.png" class="d-block w-100" alt="...">
    		</div>
  		</div>

  		<a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
    		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
    		<span class="sr-only">Previous</span>
  		</a>

  		<a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
    		<span class="carousel-control-next-icon" aria-hidden="true"></span>
    		<span class="sr-only">Next</span>
  		</a>
	</div>
</div>




	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p style="margin-bottom:-20px; font-size:28px; color:black;" class="">Noticias recientes:</p>
			</div>
		</div>
	</div>

	<div style="margin-bottom:0px; padding-bottom:0px;" class="noticias container">
		<div class="row">
			<div class="col-md-6">
				<a class="noticias-index" href="31-julio.php">
					<p class="noticias-index" style="height:90px; font-size:18px; margin-bottom:0px;">
						Solución al "Error del controlador o CpAccel al iniciar sesión en vCloudPoint S100/V1".
					</p>
				</a>
				<p class="h6" style="font-size:12px;">31 de Julio del 2019</p>
			</div>
			<div class="col-md-6">
				<a class="noticias-index" href="18-abril.php">
					<p class="noticias-index" style="height:90px; font-size:18px; margin-bottom:0px;">
						Conoces las diferencias entre RDS y VDI.
					</p>
				</a>
				<p class="h6" style="font-size:12px;">24 de Junio del 2019</p>
			</div>
			
			<hr>
		</div>
	</div>

    <div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2>Computación compartida - no es solo ahorro en costos</h2>
			</div><!--.col-12-->
		</div><!--row-->
    </div>

	<div class="container" style="min-height:600px;">
		<div class="row">
			<hr>
			<div  class="col-md-2">
				<i class="fas fa-users"></i>
			</div>
			<div class="col-md-4">
				<a href="reflejos.php"><h4>Usuarios Independientes</h4></a>
				<p style="font-size:14px;" class="text-justify">
					Cada usuario cuenta con su propio espacio de trabajo. El administrador puede configurar la 
					interfaz de cada uno de ellos.
				</p>
			</div>
			<div class="col-md-2">
				<i class="fas fa-tachometer-alt"></i>
			</div>
			<div class="col-md-4">
				<a href="reflejos.php"><h4>Rendimiento Optimizado</h4></a>
				<p style="font-size:14px;" class="text-justify">
					vCloudPoint ofrece eficiente protocolo (DDP) para soporte multimedia y dispositivos USB.
				</p>
			</div>
		</div>
	
		<div class="row">
			<div  class="col-md-2">
				<i class="fas fa-video"></i>
			</div>
			<div class="col-md-4">
				<a href="reflejos.php"><h4>Multimedia</h4></a>
				<p style="font-size:14px;" class="text-justify">
					Con vCloudPoint reproduzca simultaneamente más de 30 videos 1080px 
					desde una PC Core i7.
				</p>
			</div>

            <div class="col-md-2">
                <i class="fas fa-th"></i>
            </div>
            <div class="col-md-4">
                <a href="reflejos.php"><h4>Virtualización de IP</h4></a>
                <p style="font-size:14px;" class="text-justify">
					La virtualización de IP de vCloudPoint funciona con los sistemas Windows Server, 
					además no requiere configuraciones complejas.
				</p>
            </div>
		</div>
	
		<div class="row">
			<div class="col-md-2">
				<i class="fab fa-windows"></i>
			</div>
			<div class="col-md-4">
				<a href="reflejos.php"><h4>Compatible con Windows 10</h4></a>
				<p style="font-size:14px;" class="text-justify">
					Soporta los Sistemas Windows más populares como son Windows 10, Multipoint Server así como
					Server 2012.
				</p>

			</div>
			<div class="col-md-2">
				<i class="fas fa-cogs"></i>
			</div>
			<div class="col-md-4">
				<a href="reflejos.php"><h4>Fácil Configuración</h4></a>
				<p style="font-size:14px;" class="text-justify">
					Cuenta con una interfaz de usuario, fácil de usar ofreciendo características para usuarios
					principiantes y avanzados. Sin configuraciones extras en el host.
				</p>
			</div>
		</div>
	</div>


<div class="row" style="background-color:#333333;">
	<div class="container">
		<div class="row" style="margin-right:15px; margin-left:15px;">
			<div class="col-md-3">
				<img class="img-fluid  pt-3 pb-3" src="img/logo vcloudpoint1.png ">
				<p class="text-justify pt-2 footer-descripcion">
					Las tecnologías vCloudPoint hacen que la informática de escritorio sea extremadamente simple y económica. Debido a la excelente
					flexibilidad y simplicidad de sus cliente cero, puede implementar, usar y administrar fácilmente escritorios para docenas
					a cientos de usuarios a una fracción de los costos.
				</p>
			</div>
		
			<div class="col-md-3">
				<p style="color:white;  font-size:20px;" class="pt-3 pb-3 text-center">Contacto</p>
				<a>
					<p class="text-center" style="font-size:13px;"><i class="far fa-envelope"></i>  ventas@vcloudpoint.com.mx</p>
				</a>
				<hr style="background-color:white;">
				<p class="text-center" style="font-size:14px;"><i class="fas fa-phone-volume"></i> 01 (55) 5615-2916</p>
			</div>

			<div class="col-md-3">
				<p style="color:white; font-family: sans-serif; font-size:20px;" class="pt-3 pb-3 text-center">Noticias recientes</p>
				<a href="18-abril.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>
						Aviso: Se solucionó el problema con la actualización del sistema de
						Windows que causaba el inicio de sesión de varios usuarios no funcionara.
					</p>
				</a>
				<p style="font-size:10px;">18 de Abril del 2019</p>
				<hr class="footer2">
				<a href="18-marzo.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>VCLOUDPOINT China fue otorgado como miembro de CEEIA y SZEEIA.</p>
				</a>
				<p style="font-size:10px;">18 de Marzo del 2019</p>
			</div>

			<div class="col-md-3">
				<br>
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:40px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="http://www.facebook.com/vCloudPointMX/"> <i class="fab fa-facebook-f"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://youtu.be/cmTRDUURWTc"><i class="fab fa-youtube"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.linkedin.com/in/vcloudpoint-mexico-86a194168/"><i class="fab fa-linkedin-in"></i></a>
					</div>
				</div>
				
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:50px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://twitter.com/vcloudmx?lang=es"><i class="fab fa-twitter"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.instagram.com/vcloudpointmx/"><i class="fab fa-instagram"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--barra-progreso-->
<script type="text/javascript">
	function animar (){
		document.getElementById("barra1").classList.toggle ("final1");
		document.getElementById("barra2").classList.toggle ("final2");
		document.getElementById("barra3").classList.toggle ("final3");

	}
	window.onload = function (){
	  animar();

	}
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?51BGgA3dFcKcI1mPpd4Un7VFblV9TgM1";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


<footer style="background-color:#262626; height: auto;">
	<div class="container">
		<div class="row">
			<p style="color:#c5c5c5; font-size:14px;" class="text-center copyright w-100">vCloudPoint 2019. Todos los derechos reservados</p>
		</div> <!--.row-->
	</div><!--.container-->
</footer>









    <script src="js/jquery.slim.js"></script>
	<script src="js/app.js"></script>
    <script src="js/menu-activo.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/menu-hover.js"></script>
    <script src="js/validar-numero.js"></script>
  </body>
</html>
