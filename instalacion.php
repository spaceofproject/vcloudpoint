<!DOCTYPE html>
<html lang="en">
	<head>
		<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
			<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|PT+Sans:400,700|Roboto:400,700,900" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
			<link rel="stylesheet" href="css/styles.css">
			<link rel="stylesheet" href="css/menu.css">
			<link rel="stylesheet" href="css/footer.css">
			<link rel="stylesheet" href="css/container-inicio.css">
			<link rel="stylesheet" href="css/noticias.css">
			<link rel="stylesheet" href="css/caracteristicas.css">
			<link rel="stylesheet" href="css/divisor.css">
			<link rel="stylesheet" href="css/separador.css">
			<link rel="stylesheet" href="css/menu-activo.css">
			<link rel="stylesheet" href="css/formulario.css">
			<link rel="stylesheet" href="css/galeria.css">
			<link rel="stylesheet" href="css/imagen-hover.css">
			<link rel="stylesheet" href="css/nav-tabs.css">
			<link rel="stylesheet" href="css/productos.css">

			<link rel="icon" href="img/logo.ico">
	</head>

	<body onselectstart="return false">
		<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="height:auto;">
			<div class="container">
				<div class="col-md-3">
					<a class="navbar-brand" href="index.php">
						<img src="img/logo vcloudpoint.png" style="margin:auto; width:200px; height:auto; padding:10px;" alt=""> 
					</a>

					<button style="border:none;  background-color:#0c96cc;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span style="background-color:#0c96cc;" class="navbar-toggler-icon">
						</span>
					</button>
				</div>

				<div class="col-md-9" style="margin-left:30px;">
					<div  class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active" style="margin-right:5px;">
								<a class="nav-link home" style="padding-right:0px; padding-left:0px;" href="index.php">
									<i class="fas fa-home"></i>
									<span class="sr-only">(current)</span>
								</a>
							</li>

							<li class="nav-item" style="margin-right:5px;">
								<a class="nav-link productos" style="padding-right:0px; padding-left:0px;" href="productos.php">
									<i class="fas fa-stop"></i>
								</a>
							</li>

							<li id="m-industria" style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#industria" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-building"></i>
								</a>
								<ul class="dropdown-menu" id="industria">
									<li>
										<a href="educacion.php">
											<i class="fa fa-graduation-cap"></i>
										</a>
									</li>

									<li>
										<a href="pymes.php">
											<i class="fas fa-briefcase"></i>
										</a>
									</li>

									<li>
										<a href="servicios.php">
											<i class="fas fa-comments"></i>
										</a>
									</li>

									<li>
										<a href="espacios-publicos.php">
											<i class="fas fa-cloud"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#soporte" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" 			aria-expanded="true">
									<i class="fas fa-user-shield"></i>
								</a>
								<ul class="dropdown-menu" id="soporte">
									<li>
										<a href="instalacion.php">
											<i class="fas fa-download"></i>
										</a>
									</li>
									<li>
										<a href="preguntas-frecuentes.php">
											<i class="far fa-question-circle"></i>
										</a>
									</li>
									<li>
										<a href="complementos.php">
											<i class="fas fa-plus"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" 
								class="dropdown">
								<a href="#" data-target="#compania" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-calendar-alt"></i>
								</a>
								<ul class="dropdown-menu" id="compania">
									<li>
										<a href="nosotros.php">
											<i class="fa fa-industry"></i>
										</a>
									</li>
									<li>
										<a href="partner.php">
											<i class="fas fa-user-tie"></i>
										</a>
									</li>
									<li>
										<a href="noticias.php">
											<i class="far fa-newspaper"></i>
										</a>
									</li>
									<li>
										<a href="contacto.php">
											<i class="fa fa-phone-square"></i>
										</a>
									</li>
								</ul>
							</li>

							<li class="" style="padding-left:10px;">
								<a id="menu-casos" style="color:#666666 ; padding-right:0px; padding-left:0px;  font-size:13px; margin-top:8px;" class="nav-link casos-exito" href="casos_exito.php">
									Casos de éxito
								</a>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

  

	<div class="row" style="background-color:#0b96ce; height:auto; margin-top:90px;">
		<div class="container">
			<p style="font-size:30px; color:white; margin-top:0px;">Guía de instalación y descargas</p>
		</div>
	</div>

	<div class="container" style="margin-bottom:10px;">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb" style="background-color:transparent;">
					<li style="margin-top:-2px!important;">
						<a style="font-size:12px; color:#8b8b8b; " href="index.php">
							<img style="width:15px; margin-top:-6px;" src="img/glyphicons/png/home.png" alt="">  Inicio 
						</a>
					</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Soporte</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Instalación</li>
				</ol>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-3  pt-4">
				
			</div>
			<div class="col-md-2  pt-4">
				<a href="instalacion.php" class="boton-productos btn  btn-outline-danger activo">
					Instalación
				</a>
			</div>
            <div class="col-md-2  pt-4">
                <a href="descargas.php" class="boton-productos btn  btn-outline-danger">
                    Descargas
                </a>
            </div>
            <div class="col-md-2  pt-4">
                <a href="documentacion.php" class="boton-productos btn  btn-outline-danger">
                    Documentación
                </a>
            </div>
			<div class="col-md-3  pt-4">
				
			</div>
            
        </div>
</div>

    <hr>


<div class="container pt-2">
    <div class="row">
        <div class="col-md-1"></div>
            <div class="col-12 col-md-10">
                <div class="menu-collapse">
                                  <p>Requisitos minimos:</p>
                                  <p style="font-size:14px;">
                                  <b>1) </b>Para entregar un rendimiento de escritorio deseable, los pls configuran su host de acuerdo con la Guía de configuración
                                  del host.
                                  Cuando se usa un único host para más de 6 puestos, se recomienda usar Gigabit LAN entre el host y el conmutador. .
                                  <br>
                                  <br>
                                  <b>2) </b>Asegúrese de que su sistema host sea una edición de Windows Server o una edición Professional,
                                  Enterprise o Ultimate de los sistemas personales de Windows (las versiones de Starter, Home y Preview de los sistemas
                                  personales de Windows no son compatibles).
                                  <br>
                                  <br>
                                  <b>3) </b>Prepare los instaladores de software , incluidos vMatrix Server Manager, actualización multiusuario RDP wrapper o CAL de Microsoft
                                  RDS (elija uno), reproductor multimedia VLC (opcional pero recomendado) y otro software requerido. Puede descargar el software
                                  requerido en la pestaña "Descarga de software" de esta página.
                                  </p>
                                  <hr>
                                  <br>
                                  <br>
                                  <br>
                                  <p style="margin-top:-30px;">Instalación</p>
                                  <p style="font-size:14px;">
                                  <b>1) </b> Software de seguridad deshabilitado durante la instalación de vMatrix Server Manager y RDP Wrapper , ya que los cambios
                                  en los controladores del sistema pueden causar errores de juicio por parte del software de seguridad.
                                  <br>
                                  <br>
                                  <b>2) </b> Ejecute el instalador de vMatrix Server Manager ; Después de la instalación, se requiere reiniciar, pero se puede
                                  hacer después de que se haya instalado todo el otro software.
                                  <br>
                                  <br>
                                  <b>3) </b> Descomprime el paquete RDP Wrapper, ejecuta el archivo install.bat o instala las CAL de Microsoft RDS si
                                  lo has comprado.
                                  <br>
                                  <br>
                                  <b>4) </b> Instale el reproductor multimedia VLC y otro software necesario ;
                                  <br>
                                  <br>
                                  <b>5) </b> Reinicie, y luego cree cuentas de usuario con vMatrix Server Manager , conecte los clientes cero y la instalación
                                  está lista.
                                  </p>
                                  <hr>
                                  <br>
                          <p>Notas:</p>
<div class="menu-collapse">
    <p class="preguntas" data-toggle="collapse" data-target="#rdp" aria-expanded="false" aria-controls="collapseExample">Acerca del contenedor RDP :</p>
    <div class="collapse" id="rdp">
        <div class="pt-4">
          <p style="font-size:14px;">
            Si no funciona en Windows 10 con la última actualización, ejecute el archivo update.bat. Si está utilizando un Windows XP, siga la guía
            para descargar otro archivo especialmente diseñado para Windows XP después de hacer clic en el archivo install.bat.
            <br><br>
            Atención: se recomienda tener el archivo termsrv.dll original con la instalación de RDP Wrapper. Si lo ha modificado antes con otras actualizaciones, puede volverse inestable y bloquearse en cualquier momento.
          </p>
        </div>
    </div>
    <br>
    <br>
    <p class="preguntas" data-toggle="collapse" data-target="#vlc" aria-expanded="false" aria-controls="collapseExample">
    Acerca de VLC Media Player :
    </p>
    <div class="collapse" id="vlc">
          <div class="">
            <p style="font-size:14px;">
            Se recomienda instalar VLC media player y configurarlo como el reproductor de vídeo por defecto para los casos en que se requiere a menudo
            la reproducción de vídeo; A partir de vMatrix 2.0, vCloudPoint introdujo una nueva característica que permite a los vídeos locales jugaron
            en el cliente cero con el reproductor VLC a ser prestados localmente por el procesador del cliente en lugar de la CPU del host.
            <br><br>
            Esta característica tiene como objetivo descargar significativamente el consumo de CPU del lado del host en la reproducción de video y ayuda a admitir
            más usuarios de video por host, especialmente en los casos en que a menudo se requiere la reproducción simultánea de video. Esta característica es
            compatible con el reproductor VLC de 2.1.5 o versiones más nuevas y funciona automáticamente después de la instalación de vMatrix 2.x.
            <br><br>
            Solo asegúrate de que estás usando las versiones correctas de vMatrix y el reproductor VLC y el reproductor VLC está seleccionado para reproducir el video,
            entonces estás listo para "guardar".
            <br><br><br>
            Nota: Dado que el contenido multimedia no se procesa en el lado del host, la utilización de esta función presenta un inconveniente: el administrador
            no puede ver el contenido multimedia dentro del reproductor VLC a través del monitoreo en el lado del host.
            </p>
          </div>
      </div>
    <br>
    <br>
    <p class=" preguntas" data-toggle="collapse" data-target="#red" aria-expanded="false" aria-controls="collapseExample">
    Trabajo en red :
    </p>
    <div class="collapse" id="red">
          <div class="pt-4">
            <p style="font-size:14px;">
            Para obtener un rendimiento deseable, se recomienda que la red entre el host y el conmutador sea una LAN gigabit Ethernet, y que la
            red entre el terminal y el host sea confiable.
            <br><br>
            Si su host accede a Internet a través de VPN o no tiene conectividad a Internet,
            configúrelo para su uso fuera de línea de acuerdo con la pregunta frecuente 04 ; de lo contrario, los clientes cero pueden desconectarse
            cada pocos minutos durante la operación
            </p>
          </div>
    </div>
    <br>
    <br>
    <p class="preguntas" data-toggle="collapse" data-target="#sistema" aria-expanded="false" aria-controls="collapseExample">
    Acerca del soporte del sistema y la licencia en un entorno multiusuario :
    </p>
    <div class="collapse" id="sistema">
          <div class="pt-4">
            <p style="font-size:14px;">
            El software vMatrix es compatible con los sistemas Windows más populares, incluido XP SP3 Pro de 32 bits (excluya la edición Home Basic), Windows 7
            de 32 bits y 64 bits (excluya la edición Home Basic), Windows 8 de 32 bits
            y 64 bits (excluya Starter edition ), Windows 8.1 32-bit y 64-bit (excluir Starter edition), Windows 10 32-bit y 64-bit (excluir Starter
            & Insider edition), Server 2003 32 bits, Server 2008R2, Server 2012, Server 2012R2, Server 2016 , Multipoint Server 2011, Multipoint Server 2012.
            Nota: Los sistemas que no son de Windows como Chrome, MAC y Linux no son compatibles.
            <br><br><br>
            Nota: vMatrix no contiene un componente o aplicación para permitir que varios usuarios accedan al host de forma simultánea, para permitir un
            inicio de sesión simultáneo de múltiples usuarios, instale RDP Wrapper o Microsoft RDS CALs.
            <br><br>
            Para autorizar correctamente los clientes cero de
            vCloudPoint en un entorno Microsoft , se recomienda que obtenga las CAL de Microsoft RDS de su agente local de Microsoft.
            </p>
          </div>
    </div>
    </div>
              </div>
            </div>
        <div class="col-md-1"></div>
    </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-1"></div>
      <div class="col-md-10">
        <hr>
        <p style="font-size:12px;">Si encuentra otros problemas durante la instalación, consulte la página <a target="_blank" href="preguntas-frecuentes.php">
          "Preguntas frecuentes"</a> en este sitio o póngase en contacto con nuestro equipo de soporte.
        </p>
      </div>
      <div class="col-1"></div>
    </div>
  </div>


<div class="row" style="background-color:#333333;">
	<div class="container">
		<div class="row" style="margin-right:15px; margin-left:15px;">
			<div class="col-md-3">
				<img class="img-fluid  pt-3 pb-3" src="img/logo vcloudpoint1.png ">
				<p class="text-justify pt-2 footer-descripcion">
					Las tecnologías vCloudPoint hacen que la informática de escritorio sea extremadamente simple y económica. Debido a la excelente
					flexibilidad y simplicidad de sus cliente cero, puede implementar, usar y administrar fácilmente escritorios para docenas
					a cientos de usuarios a una fracción de los costos.
				</p>
			</div>
		
			<div class="col-md-3">
				<p style="color:white;  font-size:20px;" class="pt-3 pb-3 text-center">Contacto</p>
				<a>
					<p class="text-center" style="font-size:13px;"><i class="far fa-envelope"></i>  ventas@vcloudpoint.com.mx</p>
				</a>
				<hr style="background-color:white;">
				<p class="text-center" style="font-size:14px;"><i class="fas fa-phone-volume"></i> 01 (55) 5615-2916</p>
			</div>

			<div class="col-md-3">
				<p style="color:white; font-family: sans-serif; font-size:20px;" class="pt-3 pb-3 text-center">Noticias recientes</p>
				<a href="18-abril.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>
						Aviso: Se solucionó el problema con la actualización del sistema de
						Windows que causaba el inicio de sesión de varios usuarios no funcionara.
					</p>
				</a>
				<p style="font-size:10px;">18 de Abril del 2019</p>
				<hr class="footer2">
				<a href="18-marzo.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>VCLOUDPOINT China fue otorgado como miembro de CEEIA y SZEEIA.</p>
				</a>
				<p style="font-size:10px;">18 de Marzo del 2019</p>
			</div>

			<div class="col-md-3">
				<br>
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:40px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="http://www.facebook.com/vCloudPointMX/"> <i class="fab fa-facebook-f"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://youtu.be/cmTRDUURWTc"><i class="fab fa-youtube"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.linkedin.com/in/vcloudpoint-mexico-86a194168/"><i class="fab fa-linkedin-in"></i></a>
					</div>
				</div>
				
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:50px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://twitter.com/vcloudmx?lang=es"><i class="fab fa-twitter"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.instagram.com/vcloudpointmx/"><i class="fab fa-instagram"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--barra-progreso-->
<script type="text/javascript">
	function animar (){
		document.getElementById("barra1").classList.toggle ("final1");
		document.getElementById("barra2").classList.toggle ("final2");
		document.getElementById("barra3").classList.toggle ("final3");

	}
	window.onload = function (){
	  animar();

	}
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?51BGgA3dFcKcI1mPpd4Un7VFblV9TgM1";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


<footer style="background-color:#262626; height: auto;">
	<div class="container">
		<div class="row">
			<p style="color:#c5c5c5; font-size:14px;" class="text-center copyright w-100">vCloudPoint 2019. Todos los derechos reservados</p>
		</div> <!--.row-->
	</div><!--.container-->
</footer>









    <script src="js/jquery.slim.js"></script>
	<script src="js/app.js"></script>
    <script src="js/menu-activo.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/menu-hover.js"></script>
    <script src="js/validar-numero.js"></script>
  </body>
</html>
