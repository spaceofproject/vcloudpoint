<!DOCTYPE html>
<html lang="en">
	<head>
		<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
			<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|PT+Sans:400,700|Roboto:400,700,900" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
			<link rel="stylesheet" href="css/styles.css">
			<link rel="stylesheet" href="css/menu.css">
			<link rel="stylesheet" href="css/footer.css">
			<link rel="stylesheet" href="css/container-inicio.css">
			<link rel="stylesheet" href="css/noticias.css">
			<link rel="stylesheet" href="css/caracteristicas.css">
			<link rel="stylesheet" href="css/divisor.css">
			<link rel="stylesheet" href="css/separador.css">
			<link rel="stylesheet" href="css/menu-activo.css">
			<link rel="stylesheet" href="css/formulario.css">
			<link rel="stylesheet" href="css/galeria.css">
			<link rel="stylesheet" href="css/imagen-hover.css">
			<link rel="stylesheet" href="css/nav-tabs.css">
			<link rel="stylesheet" href="css/productos.css">

			<link rel="icon" href="img/logo.ico">
	</head>

	<body onselectstart="return false">
		<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="height:auto;">
			<div class="container">
				<div class="col-md-3">
					<a class="navbar-brand" href="index.php">
						<img src="img/logo vcloudpoint.png" style="margin:auto; width:200px; height:auto; padding:10px;" alt=""> 
					</a>

					<button style="border:none;  background-color:#0c96cc;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span style="background-color:#0c96cc;" class="navbar-toggler-icon">
						</span>
					</button>
				</div>

				<div class="col-md-9" style="margin-left:30px;">
					<div  class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active" style="margin-right:5px;">
								<a class="nav-link home" style="padding-right:0px; padding-left:0px;" href="index.php">
									<i class="fas fa-home"></i>
									<span class="sr-only">(current)</span>
								</a>
							</li>

							<li class="nav-item" style="margin-right:5px;">
								<a class="nav-link productos" style="padding-right:0px; padding-left:0px;" href="productos.php">
									<i class="fas fa-stop"></i>
								</a>
							</li>

							<li id="m-industria" style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#industria" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-building"></i>
								</a>
								<ul class="dropdown-menu" id="industria">
									<li>
										<a href="educacion.php">
											<i class="fa fa-graduation-cap"></i>
										</a>
									</li>

									<li>
										<a href="pymes.php">
											<i class="fas fa-briefcase"></i>
										</a>
									</li>

									<li>
										<a href="servicios.php">
											<i class="fas fa-comments"></i>
										</a>
									</li>

									<li>
										<a href="espacios-publicos.php">
											<i class="fas fa-cloud"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#soporte" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" 			aria-expanded="true">
									<i class="fas fa-user-shield"></i>
								</a>
								<ul class="dropdown-menu" id="soporte">
									<li>
										<a href="instalacion.php">
											<i class="fas fa-download"></i>
										</a>
									</li>
									<li>
										<a href="preguntas-frecuentes.php">
											<i class="far fa-question-circle"></i>
										</a>
									</li>
									<li>
										<a href="complementos.php">
											<i class="fas fa-plus"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" 
								class="dropdown">
								<a href="#" data-target="#compania" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-calendar-alt"></i>
								</a>
								<ul class="dropdown-menu" id="compania">
									<li>
										<a href="nosotros.php">
											<i class="fa fa-industry"></i>
										</a>
									</li>
									<li>
										<a href="partner.php">
											<i class="fas fa-user-tie"></i>
										</a>
									</li>
									<li>
										<a href="noticias.php">
											<i class="far fa-newspaper"></i>
										</a>
									</li>
									<li>
										<a href="contacto.php">
											<i class="fa fa-phone-square"></i>
										</a>
									</li>
								</ul>
							</li>

							<li class="" style="padding-left:10px;">
								<a id="menu-casos" style="color:#666666 ; padding-right:0px; padding-left:0px;  font-size:13px; margin-top:8px;" class="nav-link casos-exito" href="casos_exito.php">
									Casos de éxito
								</a>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

  

	<div class="row" style="background-color:#0b96ce; height:auto; margin-top:90px;">
		<div class="container">
			<p style="font-size:30px; color:white; margin-top:0px;">Preguntas frecuentes</p>
		</div>
	</div>

	<div class="container" style="margin-bottom:10px;">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb" style="background-color:transparent;">
					<li style="margin-top:-2px!important;">
						<a style="font-size:12px; color:#8b8b8b; " href="index.php">
							<img style="width:15px; margin-top:-6px;" src="img/glyphicons/png/home.png" alt="">  Inicio 
						</a>
					</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Soporte</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Preguntas Frecuentes</li>
				</ol>
			</div>
		</div>
	</div>

<div class="container pt-5">
  <div class="row">
    <div class="col-md-1"></div>
        <div class="col-12 col-md-6">
            <p style="margin-bottom:10px; font-size:13px;" id="preguntas" class="preguntas" data-toggle="collapse" data-target="#uno" aria-expanded="true">
01. Guía de configuración de host para la solución de computación compartida de vCloudPoint</a>
</p>
<div class="collapse" id="uno">
  <p style="font-size:13px;">Descargue la <a target=_blank href="descargas/Guia-rapida-de-instalacion-S100.pdf">guía rapida de configuración.</a></p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas"  data-toggle="collapse" data-target="#tres" aria-expanded="false" aria-controls="collapseExample">
02. Uso de Windows Active Directory (Dominio) con vCloudPoint Shared Computing
</p>
<div class="collapse" id="tres">
  <p style="font-size:13px;">
    1, su sistema de dominio debe incluir al menos un controlador de dominio y un host compartido que se haya unido al sistema de dominio.
    <br><br>
    2, instale vMatrix Server Manager en los hosts compartidos.
    <br><br>
    3, crea usuarios en el controlador de dominio.
    (Nota: como los usuarios no se crean mediante vMatrix Server Manager, debe agregar manualmente los usuarios no administradores creados al grupo Usuarios de escritorio remoto).
    <br><br>
    4, inicie sesión en el host con vCloudPoint cero clientes. Los hosts se reconocerán automáticamente si se han unido al dominio. La siguiente imagen muestra dos opciones de inicio de sesión: el primer nombre de dominio "CLOUDPOINT" se elige para iniciar sesión en el dominio; la segunda "Cuenta Local" es para iniciar sesión en la cuenta local. Si los usuarios eligen el inicio de sesión de la cuenta local, inician sesión en el host en lugar del dominio.
    <br><br>
    5, seleccione un dominio y luego ingrese su nombre de usuario y contraseña.
    <br>
    <br>
    <b>NOTA IMPORTANTE:</b>
    <br>
    1, no puede iniciar sesión en una cuenta de administrador de dominio  desde un cliente cero. Esto causará la falta de disponibilidad de todo el sistema vMatrix y cero cliente. Si hace esto por error, elimine los administradores del grupo vMatirxServerRemoteUsers en los hosts compartidos, pensó Computer Management -> Usuarios locales y grupos -> groups-> vMatirxServerRemoteUsers group
    <br><br>
    2, Doman NTLM no es compatible en este momento
    <br>
    <br>
    Nota:
    <br>
    1. De forma predeterminada, solo los usuarios en el Grupo de administrador pueden iniciar sesión en el Controlador de dominio remoto, los usuarios en el Grupo de usuarios de Escritorio remoto no pueden.
    <br>
    <br>
    2. Si está utilizando una versión de vMatrix de 1.6.0.4 o anterior, debe instalar vMatrix en el controlador de dominio y en los hosts. Si no desea instalar vMatrix en el controlador de dominio, utilice la versión vMatrix de 1.6.0.5 o posterior.
  </p>
  <img src="img/preguntas/1.jpg" class="img-fluid">
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#cuatro" aria-expanded="false" aria-controls="collapseExample">
03. Cero clientes se desconectan cada pocos minutos (utilizando cero clientes sin conectividad a Internet)
</p>
<div class="collapse" id="cuatro">
    <p style="font-size:13px;">
      Por defecto de fábrica, los clientes cero de vCloudPoint y el software vMatrix Server Manager están configurados para ser utilizados en un entorno conectado a Internet (WAN). Si su host tiene Internet, no se requiere ninguna configuración adicional. El modo de operación que se muestra en la página de administración de usuarios de cualquier dispositivo conectado al host cambiará automáticamente de "Sin conexión" en rojo a "En línea" en negro, lo que significa que el dispositivo funciona correctamente en un entorno provisto por Internet.
      <br><br>
      <img class="img-fluid" src="img/preguntas/2.png" alt="">
      <br><br>
      Sin embargo, si el Modo de operación de los dispositivos que se conectan al host permanece "Sin conexión"
      en rojo todo el tiempo, debe volver a configurar los dispositivos para su uso fuera de línea, de lo contrario,
      los dispositivos pueden desconectarse cada pocos minutos durante la operación. Los casos de uso donde puede encontrar
      este problema y necesita solicitar "Uso fuera de línea" normalmente incluyen los siguientes:
      <br><br>
      no proporciona conexión a Internet (WAN) al host o la conexión a Internet es extremadamente poco confiable;
      <br><br>
      utiliza un proxy o VPN o software de control de Internet para que el host no pueda acceder a nuestro servidor de
      configuración: api.cpterm.com.
      <br><br>
      <b>Cómo volver a configurar los dispositivos para el uso fuera de línea:</b>
      <br><br>
      1) En un entorno de conexión que no sea de Internet, vMatrix Server Manager solicitará una ventana de mensaje para la configuración de uso sin conexión dentro de los 5 minutos posteriores al inicio del host.
      uso fuera de línea 3
      <br><br>
      <img class="img-fluid" src="img/preguntas/3.png" alt="">
      <br><br>
      2) Abra el Administrador del servidor vMatrix, vaya a la página Uso fuera de línea (esta página solo aparece cuando el host no cuenta con conexión a Internet al arrancar el sistema).uso fuera de línea 4
      <br><br>
      <img class="img-fluid" src="img/preguntas/4.png" alt="">
      <br><br>
      3) Exportar el perfil de configuración. Antes de exportar, conecte todos los dispositivos del cliente para el uso fuera de línea al host, para que los números de serie (SN) se recopilen en el archivo. Alternativamente, puede anotar las SN si no puede conectar todos los dispositivos del cliente (especialmente en implementaciones grandes). El perfil de host contiene información de hardware del host. Por lo tanto, asegúrese de que el hardware del host, por ejemplo, CPU, memoria, unidades y tarjeta de red, sean exactamente iguales a su condición de uso real sin conexión, de lo contrario, si hay algún cambio en el hardware host, la configuración fuera de línea generada será inválida. al anfitrión.
      <br><br>
      4) El distribuidor le devolverá un archivo de configuración basado en su último perfil exportado. Importe el archivo devuelto para completar.
      <br><br>
      5) Si su configuración para el uso fuera de línea se lleva a cabo con éxito, el modo de operación cambia de "Sin conexión" en rojo a "Sin conexión" en negro.
      <br><br>
      Nota: la configuración de uso sin conexión en vMatrix Server Manager se introdujo en el lanzamiento de la versión de vMatirx Server Manager 2.0.2. Si planea utilizar los clientes cero de vCloudPoint en un entorno de Internet no interno o inestable, utilice 2.0.2 o una versión posterior de vMatrix Server Manager, y póngase en contacto con el distribuidor para generar un archivo de configuración fuera de línea.
    </p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas"  data-toggle="collapse" data-target="#cinco" aria-expanded="false" aria-controls="collapseExample">
04. El host no se muestra en la lista de host en el menú de inicio de sesión del cliente cero.
</p>
<div class="collapse" id="cinco">
  <p style="font-size:13px;">
    1) El software vMatrix Server Manager no está instalado en el host o no funciona correctamente.
    <br><br>
    Resolución: instale o reinstale vMatrix Server Manager.
    <br>
    <br>
    2) El host o los clientes cero no están conectados a la LAN.
    <br><br>
    Resolución: conecte el host y los clientes cero a la LAN y asegúrese de que la red esté bien.
    <br>
    <br>
    3) El cliente cero está con una nueva versión de firmware mientras que la versión de vMatrix Server Manager en el host está desactualizada.
    <br>
    Resolución: Actualice el Administrador del servidor vMatrix.
    <br>
    <br>
    4) Las direcciones IP en el grupo de DHCP se han agotado para que los nuevos clientes cero conectados no puedan encontrar hosts en la LAN mientras que los clientes cero conectados anteriormente no tienen el problema.
    <br><br>
    Resolución: vaya a la página de configuración de DHCP y habilite más direcciones IP o acorte el tiempo de tenencia de la dirección IP.
  </p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#seis" aria-expanded="false" aria-controls="collapseExample">
05. Los clientes cero no pueden conectarse al host seleccionado.
</p>
<div class="collapse" id="seis">
  <p style="font-size:13px;">
    Por favor, resuelva el problema de acuerdo con el aviso si hay, si no hay, refiérase a los siguientes.
    <br><br>
    1) Una ventana emergente indica que el número de serie del dispositivo no es válido.
    <br><br>
    Resolución: comuníquese con nuestro equipo de soporte técnico con números de serie.
    <br><br>
    2) red de área local (LAN) inestable.
    <br><br>
    Resolución: Examine el cable y el conmutador y asegúrese de que los clientes cero estén conectados al host en la misma LAN.
    <br><br>
    3) El grupo de direcciones IP está lleno para que los nuevos dispositivos no puedan iniciar sesión.
    <br><br>
    Resolución: alquile más direcciones IP y recomiende acortar el tiempo de tenencia de la dirección IP en caso de que haya muchos dispositivos móviles conectados a la misma red.
    <br><br>
    4) conflicto de dirección IP que el inicio de sesión de un usuario puede registrar el otro.
    <br><br>
    Resolución: asegúrese de que todos los dispositivos utilicen direcciones IP diferentes de las demás en la misma LAN. Recomiende usar DHCP predeterminado en lugar de direcciones IP estáticas.
  </p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#siete" aria-expanded="false" aria-controls="collapseExample">
06. Conexión de dispositivos USB al cliente cero de vCloudPoint.
</p>
<div class="collapse" id="siete">
  <p style="font-size:13px;">
    <b>Dispositivos USB que se prueban para trabajar en los clientes cero de vCloudPoint:</b>
    <br><br>
    Dispositivos de almacenamiento, concentradores USB, impresoras multifuncionales y de matriz de
    puntos , lector de tarjetas inteligentes, escáneres de oficina y TPV, pantallas táctiles simples, teclas U, convertidores
    paralelo a USB, etc .;
    <br>
    Es posible que se requiera una fuente de alimentación externa para los dispositivos que trabajan con una gran tensión.
    <br><br>
    <b>Uso de dispositivos USB con cero clientes de vCloudPoint:</b>
    <br><br>
    Para usar dispositivos USB con clientes cero de vCloudPoint, simplemente instaló el controlador de dispositivo nativo en el sistema host como lo hace normalmente cuando usa PC. NO se requieren controladores adicionales para los clientes cero. La tecnología de redirección de vCloudPoint USB permite que los dispositivos USB funcionen tanto en los clientes cero como en el host.
    <br><br>
    Todos los usuarios clientes pueden acceder a los dispositivos de impresión y almacenamiento conectados al host compartido. Los demás usuarios pueden acceder a los dispositivos de impresión conectados al cliente cero, pero los dispositivos de almacenamiento conectados al cliente cero solo pueden acceder al usuario actual debido a la tecnología vCell User Isolation de vCloudPoint.
  </p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px; align:justify;" class="preguntas" data-toggle="collapse" data-target="#ocho" aria-expanded="false" aria-controls="collapseExample">
07. Las unidades USB y de audio no se pueden instalar en sistemas operativos Windows10, 8.1, 8 y 7 sin conexión a Internet.
</p>
      <div class="collapse" id="ocho">
        <p style="font-size:13px;">
          En los sistemas operativos Windows 10, 8.1, 8 y 7, todos los controladores y programas deben estar firmados digitalmente
          (verificados) para poder ser instalados. Si no tiene conexión a Internet por primera vez en la instalación del software
          del servidor vMatrix, debe desactivar la Aplicación de la firma del controlador de Windows. De lo contrario, los controladores
          USB y de audio no se pueden ejecutar por error al obtener la verificación de la firma.
          <br><br>
          Siga estos pasos para deshabilitar la Aplicación de la firma del controlador en Windows 10, Windows 8.1,
          Windows 8, Windows 7, Windows Server 2008 R2 una vez.
          <br><br>
          <h3>Paso 1. Entrar en el menú de Opciones Avanzadas.</h3>
          <br><br>
          Para ingresar al menú Opciones avanzadas en Windows 8 y 8.1 OS:
          <br>
          1. Presione las teclas " Windows " + " R " para cargar el cuadro de diálogo Ejecutar .
          <br><br>
          2. Escriba " shutdown / r / o " y presione Enter .
          <br><br>
          <img class="img-fluid" src="img/preguntas/5.png" alt="">
          <br><br>
          3. Windows le informa que está a punto de cerrar la sesión. Presione "Cerrar".
          <br><br>
          <img class="img-fluid" src="img/preguntas/6.jpg" alt="">
          <br><br>
          4. Cuando Windows se reinicie, presione  " Solucionar problemas ".
          <br><br>
          <img class="img-fluid" src="img/preguntas/7.png" alt="">
          <br><br>
          5. En la pantalla " Opciones de solución de problemas ", elija " Opciones avanzadas ".
          <br><br>
          <img class="img-fluid" src="img/preguntas/8.png" alt="">
          <br><br>
          6. En la ventana " Opciones avanzadas ", elija " Configuración de inicio ".
          <br><br>
          <img class="img-fluid" src="img/preguntas/9.png" alt="">
          <br><br>
          7. En la pantalla " Configuración de inicio ", haga clic en " Reiniciar ".
          <br><br>
          <img class="img-fluid" src="img/preguntas/10.png" alt="">
          <br><br>
          8. Después de reiniciar en las ventanas de " Configuración de inicio ", presione la tecla " F7 " (o la tecla " 7 ") en su teclado para " Desactivar el cumplimiento de la firma del controlador ".
          <br><br>
          <img class="img-fluid" src="img/preguntas/11.jpg" alt="">
          <br><br>
          Tu computadora se reiniciará nuevamente.
          <br><br>
          9. Después del reinicio, proceda a instalar su controlador sin firmar. Durante el procedimiento de instalación, Windows le informará que no puede verificar el editor de este software del controlador. En este punto, ignore el mensaje de advertencia y elija "Instalar este software de controlador de todos modos " para completar la instalación.
          <br><br>
          <img class="img-fluid" src="img/preguntas/12.jpg" alt="">
        </p>
      </div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#nueve" aria-expanded="false" aria-controls="collapseExample">
08. Todos los dispositivos USB, el audio del usuario no puede funcionar y el rendimiento del video es malo (error cpaccel.exe).
</p>
<div class="collapse" id="nueve">
  <p style="font-size:13px;">
    <b>¿Por qué todos los dispositivos USB, el audio del usuario no pueden funcionar o los
    videos no se pueden reproducir sin problemas?</b>
  <br>
  <br>
    Si tiene uno o todos los problemas anteriores, probablemente vea el mensaje de error "cpaccel.exe" (Cloudpoint Multimedia Accelerator) de Windows o ejecute vMatrix Diagnostic Tool. Esto se debe a la falta de algún archivo de vMatrix Server Manager, principalmente causado por una eliminación incorrecta o un bloqueo de firewall o software antivirus. La mejor y más rápida forma de solucionar este problema es volver a instalar vMatrix Server Manager (asegúrese de haber desactivado el software de seguridad antes de la instalación). Si todavía tiene este problema en uso, es posible que deba agregar archivos vMatrix o puertos a la lista de excepciones del software de seguridad. Consulte también el  software Cómo configurar Firewall y Antivirus para productos vCloudPoint en esta página.
  </p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:14px;" class="preguntas" data-toggle="collapse" data-target="#diez" aria-expanded="false" aria-controls="collapseExample">
9. Compatibilidad de aplicaciones en el entorno multiusuario.
</p>
<div class="collapse" id="diez">
  <p style="font-size:14px;">
    <b>¿Qué software se puede ejecutar o no en los clientes cero de vCloudPoint?</b>
    <br>
    1, el software Windows más popular se puede ejecutar con clientes cero de vCloudPoint como aplicaciones de Office, Skype, Outlook, Chrome, Firefox, IE, Edge, Teamviewer, Adobe Photoshop, Adobe Illustrator, Adobe After Effects, Adobe Indesign, AutoDesk CAD y Camtasia , etc.
    <br><br>
    2, los juegos 3D no son compatibles. El software de diseño 3D como 3D Max se puede ejecutar, pero la renderización lleva mucho tiempo. Es posible que no se admita otro software 3D ya que los clientes cero no utilizan la tarjeta gráfica en el procesamiento. La experiencia de escritorio en el cliente cero es como la de la computadora host sin una tarjeta gráfica discreta.
    <br><br>
    3, algunos software no pueden funcionar en el entorno multiusuario.
    Se sabe que Adobe Premiere y VPN no son compatibles en el entorno multiusuario. Para saber si su software deseado se puede ejecutar en el entorno multiusuario sin cero clientes, puede intentar ejecutar el software en sesiones de escritorio remoto con PC (ejecute "mstsc").
    <br><br>
    <b>Problemas al rootear algunas aplicaciones que se supone que son compatibles,
    pero que aún no se pueden ejecutar con los clientes cero de vCloudPoint:</b>
    <br><br>
    1, La partición de disco donde se instalan las aplicaciones debe configurarse para que sea visible para los usuarios de la terminal. Para configurar las particiones de discos visibles, abra vMatrix Server Manager, haga clic en Configuración y luego en Visibilidad de almacenamiento, marque la partición donde están instaladas sus aplicaciones.
    <br><br>
    2, intente deshabilitar o habilitar Windows UAC. Algunos programas, especialmente software de seguridad o administración, pueden requerir autorización de administrador cuando Windows UAC está habilitado. Por lo tanto, cuando un programa no administrador inicia el software, solicita al usuario que continúe con la autorización del administrador. En este caso, debe deshabilitar UAC para usuarios de terminal con nivel de cuenta de usuario normal. Mientras que algunos software, especialmente el software comercial, como el famoso software chino, Foxmail y Fangyou, se diseñaron sin considerar el entorno multiusuario que todos los datos del usuario se almacenan en la misma carpeta, lo que genera interferencia entre los usuarios. Para este software, debe habilitar el UAC de Windows para que los datos de cada usuario se redirijan a diferentes carpetas.
    <br><br>
    3, cambie el directorio de instalación a no basado en el usuario. El directorio de instalación predeterminado de algún software como Kingsoft WPS está basado en el usuario, como "C: \ Users \ Administrator". Al realizar la instalación, debe cambiarla a una que no esté basada en el usuario, como "C: \ Archivos de programa \", de lo contrario, otros usuarios no podrán ejecutar este software.
  </p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#once" aria-expanded="false" aria-controls="collapseExample">
  10. Agregar usuarios creados por el sistema al grupo de usuarios remotos del servidor vMatrix (grupo de usuarios de escritorio remoto)
</p>
<div class="collapse" id="once">
  <p style="font-size:13px;">
    De forma predeterminada, el grupo Usuarios de escritorio remoto no está lleno. Debe decidir qué usuarios y grupos deben tener permiso para iniciar sesión de forma remota y luego agregarlos manualmente al grupo. Particularmente con vCloudPoint: los usuarios creados con vMatrix Server Manager se agregan automáticamente al grupo de usuarios de escritorio remoto, pero si los usuarios no se crean con vMatrix Server Manager, por ejemplo, los usuarios creados a partir del sistema o dominio, y no son administradores, entonces usted tiene que agregarse manualmente al grupo de usuarios de Escritorio remoto.
    <br><br>
    Open Computer Management. Para abrir Administración de equipos, haga clic en <b>Inicio</b>  , haga clic
    en <b>Panel de control</b> , haga doble clic en <b>Herramientas administrativas</b>  y luego haga doble clic en
    <b>Administración de equipos</b> .
    <br>
    En el árbol de la consola, haga clic en el nodo <b>Usuarios y grupos locales</b>  .
    <br>
    En el panel de detalles, haga doble clic en la carpeta <b>Grupos</b> .
    <br>
    Haga doble clic en <b>Usuarios de escritorio remoto</b> y haga clic en <b>Añadir</b>  ... .
    <br>
    En el cuadro de diálogo <b>Seleccionar usuarios</b>  , haga clic en <b>Ubicaciones</b> ... para especificar la ubicación de búsqueda
    <br>
    Haga clic en <b>Tipos de objetos</b>  ... para especificar los tipos de objetos que desea buscar.
    <br>
    Haga clic en <b> Comprobar nombres;</b> Cuando se encuentre el nombre, haga <b>clic</b> en <b>Aceptar</b> .
    <br><br>
    Nota importante:  para iniciar sesión remotamente en el sistema local, los usuarios deben estar en el grupo Usuarios de escritorio remoto, por lo tanto, los usuarios creados o usuarios de dominio deben agregarse manualmente en el grupo Usuarios de escritorio remoto pero NO en el grupo Usuarios remotos de vMatrix.
  </p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#doce" aria-expanded="false" aria-controls="collapseExample">
11. Configuración de firewall y software antivirus para productos vCloudPoint.
</p>
<div class="collapse" id="doce">
  <p style="font-size:13px;">
    <b>Configuración de Firewall y software antivirus para productos vCloudPoint</b>
    <br><br>
    El antivirus, el firewall y otros tipos de software de seguridad a veces pueden interferir con la configuración inicial o la operación
    continua del software vMatrix de vCloudPoint. Este documento proporciona información básica sobre aplicaciones, servicios y comunicación
    de red dentro de vMatrix, que se puede utilizar para configurar el software de seguridad y ayudar a garantizar la compatibilidad y la
    operación estable y continua.
    <br><br>
    <b>Instalación de vMatrix</b>
    <br><br>
    Asegúrese de DESHABILITAR cualquier software de Anti-Virus o Firewall durante la instalación de nuestro producto.
    El software de este tipo ha sido probado y se sabe que interfiere con la instalación de nuestro producto. Una vez completada la
    instalación, puede volver a habilitar el software Anti-Virus y Firewall.
    <br><br>
    Si se produce inestabilidad del sistema después de instalar vMatrix, intente eliminar vMatrix y cualquier antivirus o software de seguridad
    presente, y luego vuelva a instalar vMatrix. Si el sistema es estable en esta configuración, vuelva a instalar el software antivirus.
    En algunos casos, este cambio en el orden de instalación puede mejorar la interacción entre vMatrix y el software antivirus. Si los problemas
    persisten, intente configurar su software de seguridad para ignorar / permitir / confiar en los siguientes puertos y ejecutables:
    <br><br>
    <b>Excepciones</b>
    <br><br>
    <b>1. Excepciones de puerto</b>
    <br><br>
    Servicios de escritorio remoto: TCP 3389
    <br>
    vMatrix Network Services: TCP 13389-13342 más 3 por usuario
    <br><br>
    UDP 13389
    <br><br>
    Estos puertos son configuraciones predeterminadas en la instalación de vMatrix, pero se pueden personalizar
    en vMatrix -> Configuración -> Puertos IP / TCP.
    <br><br>
    <b>2. Excepciones de acceso a archivos</b>
    <br><br>
    <b>En Firewall:</b>
    <br><br>
    C: \ Archivos de programa \ Cloudpoint \ vMatrix \ CpDaemon.exe
    <br>
    C: \ Archivos de programa \ Cloudpoint \ vMatrix \ Driver \ CpAccel.exe
    <br><br>
    <b>En el antivirus:</b>
    <br><br>
    - Por archivos específicos:
    <br><br>
    - Para la funcionalidad de vMatrix
    <br>
    C: \ Archivos de programa \ Cloudpoint \ vMatrix \ CpDaemon.exe
    <br>
    C: \ Archivos de programa \ Cloudpoint \ vMatrix \ Driver \ CpAccel.exe
    <br><br>
    - Para el icono de la bandeja vMatrix y la funcionalidad del usuario
    <br>
    C: \ Archivos de programa \ Cloudpoint \ vMatrix \ CpDeploy.exe
    <br><br>
    - Para el administrador de administración de vMatrix
    <br>
    C: \ Archivos de programa \ Cloudpoint \ vMatrix \ CpManager.exe
    <br><br>
    - Para las herramientas de diagnóstico vMatrix
    C: \ Program Files \ Cloudpoint \ vMatrix \ DiagnosticTools.exe
    <br><br>
    - Para vMatrix Instalar y actualizar herramientas de implementación
    C: \ Archivos de programa \ Cloudpoint \ vMatrix \ InstallDeployTools.exe
    <br><br>
    - Por carpeta:
    <br><br>
    - Para todas las funciones de vMatrix
    C: \ Program Files \ Cloudpoint \ vMatrix
  </p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas"  data-toggle="collapse" data-target="#trece" aria-expanded="false" aria-controls="collapseExample">
12. Conexión remota de clientes cero de vCloudPoint a través de WAN (Internet cruzado).
</p>
<div class="collapse" id="trece">
<p style="font-size:13px;">
  Para la conexión remota con clientes cero de vCloudPoint a través de WAN, configurar el host
  DMZ es la manera más fácil pero insegura, por lo tanto, puede agregar estos puertos en su enrutador.
  <br><br>
  El servicio de red vMatrix usa puertos TCP y UDP. El puerto de difusión se fija mediante UDP 13389,
  y el puerto de escucha comienza con TCP 13389 y se extiende a al menos 3 extra más 3 por cada usuario.
  Por ejemplo, si hay 10 usuarios que necesitan conectarse de forma remota, debe al menos agregar puertos
  de UDP 13389-13389, TCP 13389-13422 (13422 es el mínimo y reservar más si es posible).
  <br><br>
  <img class="img-fluid" src="img/preguntas/12.png" alt="">
  <br><br>
  El puerto de escucha para el servicio de escritorio remoto también debe estar abierto. Utiliza TCP 3389.
  <br><br>
  Estos puertos son configuraciones predeterminadas en la instalación de vMatrix pero pueden personalizarse en vMatrix -> Configuración -> Puertos IP / TCP.
  <br><br>
  <b>Nota:</b>
  <br><br>
  1.- Debido al ancho de banda limitado y la alta latencia de la mayoría de las visitas de Internet cruzadas, el rendimiento del escritorio puede verse significativamente comprometido. Si planea conectar a los clientes cero en este enfoque de forma permanente, asegúrese de que tanto el lado del cliente como el lado del host tengan una condición de Internet eficiente para entregar el rendimiento de escritorio deseable.
  <br>
  2.-Solo un host vMatrix es compatible en la LAN para la conexión a internet cruzada.
</p>
</div>
<hr>
            <p style="margin-bottom:10px;font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#catorce" aria-expanded="false" aria-controls="collapseExample">
13. Actualización del firmware de cliente cero.
</p>
<div class="collapse" id="catorce">
<p style="font-size:13px;">
  Siempre se recomienda utilizar la última versión disponible de firmware en su cliente cero de vCloudPoint. Cada instalación de vMatrix Server Manager incluye el último firmware, por lo que no se necesitan descargas adicionales para completar este proceso. Cuando está encendido, el cliente cero establece la conexión con vMatrix Server Manager y solicita una actualización si hay una nueva versión disponible para la actualización.
</p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#quince" aria-expanded="false" aria-controls="collapseExample">
14. Restablecer los clientes cero de vCloudPoint.
</p>
<div class="collapse"id="quince">
<p style="font-size:13px;">
  Los clientes cero de vCloudPoint como S100 proporcionan dos métodos diferentes para restablecer las configuraciones del dispositivo:
  <br><br>
  1. Restablecimiento de configuraciones personalizadas: presione F2 en el inicio del dispositivo.
  <br><br>
  Esto es para eliminar configuraciones personalizadas como el nombre de usuario guardado, la contraseña, la resolución, la imagen de fondo y la red, etc. Esto se usa a menudo para restaurar la resolución de escritorio que está fuera del alcance del monitor a 1027 × 768.
  <br><br>
  2. Restablecimiento del firmware: mantenga presionado el botón del interruptor hasta que vea la ventana de reinicio.
  <br><br>
  Esto es para restablecer el firmware del dispositivo a uno instalado de fábrica. Todas las configuraciones se recuperarán a los valores predeterminados de fábrica. Esto se usa a menudo cuando el  sistema del dispositivo se vuelve defectuoso o se daña por una actualización incorrecta del firmware.
</p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#dieciseis" aria-expanded="false" aria-controls="collapseExample">
15. Conexión de cero clientes de vCloudPoint al host a través de una red inalámbrica (WIFI).
</p>
<div class="collapse" id="dieciseis">
<p>
  Si adquirió un modelo que no es WIFI de clientes cero de vCloudPoint, pero luego desea trabajar en el entorno inalámbrico, puede convertir
  el cliente cero en uno compatible con WIFi simplemente conectando una antena USB externa compatible al cliente cero de vCloudPoint. Hay dos formas de obtener la antena USB externa.
  <br><br>
  1.- puede comprar la antena USB externa de su distribuidor local de vCloudPoint.
  <br><br>
  <img class="img-fluid" src="img/antena-wifi.png" alt="">
  <br><br>
  2, cualquier antena que esté construida con el chip RTL8188EUS y se conecte con el estándar USB 2.0 es compatible con los clientes cero de vCloudPoint.
  <br><br>
  Dado el golpe es la especificación de la antena USB externa:
  <br><br>
  <table class="table table-hover">
                          <caption>vCloudPoint</caption>
                          <tbody>
                                <tr>
                                  <td colspan="3">Conexión</td>
                                  <td colspan="3">USB2.0</td>
                                </tr>
                                <tr>
                                  <td colspan="3">Chip</td>
                                  <td colspan="3">RTL8188EUS</td>
                                </tr>
                                <tr>
                                  <td colspan="3">Antena</td>
                                  <td colspan="3">Antena externa de 5dBi</td>
                                </tr>
                                <tr>
                                  <td colspan="3">Antena estándar</td>
                                  <td colspan="3">IEEE 802.11g <br>
                                      IEEE 802.11b <br>
                                      IEEE 802.11n
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="3">Velocidad</td>
                                  <td colspan="3">11b: 1/2 / 5.5 / 11 Mbps <br>
                                      11g: 6/9/12/18/24/36/48/54 Mbps <br>
                                      11n: hasta 150 Mbps
                                  </td>
                                  <td colspan="3"></td>
                                </tr>
                                <tr>
                                  <td colspan="3">Distancia</td>
                                  <td colspan="3">hasta 200 metros bajo techo</td>
                                </tr>
                                <tr>
                                  <td colspan="3">Rango de frecuencia</td>
                                  <td colspan="3">	2.4 ~ 2.4835GHZ</td>
                                </tr>
                                <tr>
                                  <td colspan="3">Science Museum</td>
                                  <td colspan="3">Gratis</td>
                                </tr>
                                <tr>
                                  <td colspan="3">Canal de trabajo</td>
                                  <td colspan="3">	1 ~ 14</td>
                                </tr>
                                <tr>
                                  <td colspan="3">Características de seguridad</td>
                                  <td colspan="">WPA-PSK / WPA2-PSK <br>
                                                WPA / WPA2 <br>
                                                Encriptación WEP 64/128 / 152bit
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="3">Poder</td>
                                  <td colspan="3">2.5w</td>
                                </tr>
                          </tbody>
                    </table>
  <br>
  <b>Pasos para configurar el WIFI externo:</b>
  <br><br>
  1, conecte la antena USB al cliente cero;
  <br><br>
  2, reinicia el cliente cero;
  <br><br>
  3, vaya a la página "Red" y verá que viene la opción WIFI. Ver fotos a continuación.
  <br><br>
  (la opción WIFI se mostrará automáticamente en la página Red una vez que se detecte el módulo WIFI en el arranque del dispositivo).
  <br><br>
  (la primera imagen muestra la página "Red" sin la opción WIFI, y la segunda muestra eso con la opción WIFI).
  <br><br>
  <img class="img-fluid" src="img/preguntas/13.jpg" alt="">
  <br><br>
  <img class="img-fluid" src="img/preguntas/14.jpg" alt="">
</p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#diecisiete" aria-expanded="false" aria-controls="collapseExample">
16. Habilitar la personalización de tema y fondo de pantalla para cero usuarios de cliente.
</p>
<div class="collapse" id="diecisiete">
<p style="font-size:13px;">
  De forma predeterminada, la personalización de tema y fondo de pantalla está deshabilitada para los usuarios de terminal a través de
  conexiones de escritorio remoto. Los administradores pueden habilitar esta opción para los usuarios de la terminal a través de unos
  simples pasos. Pero habilitar esta opción aumentará el consumo de grandes recursos en el host.
  <br><br>
  Estos son los pasos para habilitar la personalización de tema y fondo de pantalla en Windows:
  <br><br>
  <b>1) para hosts que ejecutan Windows Client Systems como XP, 7, 8, 8.1, 10 (las características de Desktop
  Experience de los sistemas cliente están preinstaladas en la instalación del sistema).
  </b>
  <br><br>
  Abra vMatrix Server Manager, en la página inicial de Administración de usuarios, haga clic con el botón derecho en el módulo de usuario y luego seleccione
  "Propiedad" para ingresar la configuración personal del usuario. (Si está configurando para todos los usuarios a la vez, vaya a "Configuraciones" -> "Configuración de usuario predeterminada").
  <br><br>
  <img class="img-fluid" src="img/preguntas/15.jpg" alt="">
  <br><br>
  Haga clic en la etiqueta "Avanzado" y luego seleccione "óptimo" para habilitar todas las opciones de experiencia de escritorio,
  incluidos fondos de pantalla, temas, animaciones de menú y contenido de la ventana al arrastrar. Para habilitar solo una opción de
  experiencia de escritorio individual como fondos de pantalla o temas, puede hacer clic en un botón "personalizado" para una mayor personalización.
  <br><br>
  <img class="img-fluid" src="img/preguntas/16.jpg" alt="">
  <br><br>
  Desplácese hacia abajo hasta "Opciones de escritorio", seleccione las opciones que desea habilitar y aplique.
  <br><br>
  <img class="img-fluid" src="img/preguntas/17.jpg" alt="">
  <br><br>
  El cambio tendrá efecto en el próximo inicio de sesión del usuario.
  <br><br>
  <b>2) para hosts que ejecutan sistemas de servidor Windows como Server 2008R2, 2012, 2012R2, 2016 y Multipoint Server 2011,
  2012 (las características de Desktop Experience para sistemas de servidor no están instaladas en la instalación del sistema).
  </b>
  <br><br>
  Para los sistemas de servidor, debe instalar las características de Experiencia de escritorio antes de configurar en vMatrix Server Manager.
  <br><br>
  Abra el Administrador del Servidor de Windows;
  <br><br>
  2) para hosts que ejecutan sistemas de servidor Windows como Server 2008R2, 2012, 2012R2, 2016 y Multipoint Server 2011,
  <br><br>
  <img class="img-fluid" src="img/preguntas/18.jpg" alt="">
  <br><br>
  Haga clic en "Agregar roles y funciones";
  <br><br>
  <img class="img-fluid" src="img/preguntas/19.jpg" alt="">
  <br><br>
  Buscar funciones -> Experiencia de escritorio;
  <br><br>
  <img class="img-fluid" src="img/preguntas/20.jpg" alt="">
  <br><br>
  Confirmar para instalar las características de Experiencia de escritorio;
  <br><br>
  <img class="img-fluid" src="img/preguntas/21.jpg" alt="">
  <br><br>
  Cuando se haya completado, reinicie el sistema y luego abra vMatrix Server Manager para habilitar las opciones de Experiencia de escritorio como las mismas para configurar para los sistemas cliente de Windows mencionados anteriormente.
</p>
</div>
<hr>
            <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#dieciocho" aria-expanded="false" aria-controls="collapseExample">
17. Desactivar ventana emergente de solicitud de permiso de administrador (lista blanca de UAC)
</p>
<div class="collapse" id="dieciocho">
  <h3>
    Deshabilite la ventana emergente de solicitar permiso de administrador para ejecutar algunas aplicaciones agregando las
    aplicaciones a la lista blanca de UAC
  </h3>
  <p style="font-size:13px;">
  <b>Descripción: por </b>
  <br><br>
  razones de seguridad y administración, se recomienda tener habilitado el UAC (Control de cuentas de usuario). Pero con el UAC habilitado,
  el UAC puede solicitar a los usuarios de cero clientes que ingresen credenciales de administrador cuando ejecutan algunas aplicaciones que
  requieren permiso de administrador. En este caso, puede agregar estas aplicaciones al whiltelist de UAC sin otorgar a los usuarios de cero
  clientes el permiso de administrador.
  <br><br>
  <b>Notas:</b>
  1) La lista blanca de UAC se usa solo cuando el UAC está habilitado. Consulte el documento de Control de cuentas de usuario para habilitar UAC.
  <br>
  2) Esta característica requiere la instalación de ApplicationCompatibilityToolkitSetup .
  <br><br>
  <b>Pasos de configuración:</b>
  <br><br>
  Instalar exe . Simplemente haga clic en Siguiente para finalizar la instalación.
  <br><br><br>
  Open Compatibility Administrator (32 bits) significa agregar una aplicación de 32 bits a la lista blanca y 64 bits significa agregar una aplicación de 64 bits a la lista blanca.
  <br><br><br>
  Haga clic con el botón secundario en Nueva base de datos , seleccione Crear nuevo , seleccione Corrección de la aplicación ...
  <br><br><br>
  Ingrese los nombres de la aplicación y del proveedor a reparar y luego haga clic en Examinar .
  <br><br><br>
  Busque la ruta de instalación de la aplicación, seleccione el ejecutable .exe y haga clic en Abrir .
  <br><br><br>
  Confirma y haz clic en Siguiente .
  <br><br><br>
  Compruebe RunAsAdmin y RunAslnvoker , haga clic en Siguiente .
  <br><br><br>
  Haga clic en Siguiente .
  <br><br><br>
  Haga clic en finalizar .
  <br><br><br>
  Haga clic en Guardar .
  <br><br><br>
  Ingrese un nombre de Base de datos en la ventana emergente, haga clic en Aceptar .
  <br><br><br>
  Ingrese un nombre de archivo, luego guárdelo.
  <br><br><br>
  Haga clic en Archivo y luego elija Instalar .
  <br><br><br>
  La aplicación ahora se ha agregado a la lista blanca de UAC y cero usuarios de clientes pueden ejecutar la aplicación sin permiso de administrador.
  <br><br><br>
</p>
</div>
<hr>
        </div>
      <div class="col-12 col-md-4 carta-preguntas">
          <div class="card ">
            <i class="fas fa-briefcase-medical"></i>
                <div class="card-body">
                    <h3 style="margin-bottom:20px; margin-top:-5px;" class="card-title">¿Aún necesitas ayuda?</h3><hr>
                    <p style="font-size:14px;" class="card-text">
                      Por favor <a href="contacto.php">envíanos un correo electrónico</a> con su pregunta o problema. Para problemas técnicos, describa en
                      detalles los mensajes de error, entorno de host como el sistema, vMatrix y las versiones de firmware del dispositivo,
                      estado de la red, pasos para volver a generar los problemas, cambios recientes que sospechó que eran la causa del problema
                      y si es posible, incluya capturas de pantalla en el archivo adjunto para ayudar a nuestro equipo de soporte a analizar y
                      presentar soluciones efectivas.
                    </p>
                </div>
          </div>
      </div>
      <div class="col-md-1"></div>
  </div>
</div>
</div>

  <div class="container">
  <div class="row">
    <div class="col-md-1"></div>
      <div class="col-md-6">
        <h3>Consejos adicionales</h3>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#con-uno" aria-expanded="false" aria-controls="collapseExample">
Consejo 01: utilice el paquete de códec VLC Player o K-lite para reducir aún más el consumo de CPU del host en la reproducción de video local
</p>
<div class="collapse" id="con-uno">
<p style="font-size:13px;">
  Cuando una sola computadora host es compartida por múltiples usuarios que ejecutan vCloudPoint cero clientes, la principal
  preocupación del administrador es cómo aprovechar al máximo los recursos de host y admitir más usuarios. Qué reproductor multimedia eliges y
  cómo lo usas para reproducir videos locales tiene un gran impacto en el consumo de CPU del host. GOM Player, KM Player, Potplayer, SMPlayer
  y Media Player Classic son algunos de los reproductores de medios populares que los clientes probablemente usarán. Estos reproductores
  multimedia tienen la mayoría de los códecs incluidos para admitir una gran cantidad de formatos de medios. Sin embargo, como algunos de
  estos reproductores de medios no son compatibles con la aceleración de hardware, puede experimentar un gran consumo de CPU al reproducir
  videos locales con ellos.
</p>
<h3>Muy recomendable</h3>
<p style="font-size:13px;"><b>Reproductor VLC que admite la representación del cliente con aceleración de hardware</b>
Para ayudar a los clientes a descargar el consumo de CPU del host en la reproducción de video y admitir más usuarios de video por host, especialmente en los casos en que a menudo se requiere reproducción de video simultánea, comenzando con la versión de vMatrix 2.0, presentamos una nueva característica de compatibilidad con renderización de clientes. Esta característica permite que los videos locales reproducidos en el cliente cero con el reproductor VLC sean procesados ​​localmente por el procesador del cliente en lugar de la CPU del host, por lo tanto, el consumo del CPU del host solo lo toma el jugador VLC a tan solo menos del 1% de un procesador i7 por video. Esta característica es compatible con el reproductor VLC de 2.1.5 o versiones más nuevas y funciona automáticamente después de la instalación de vMatrix 2.x. Solo asegúrate de que estás usando las versiones correctas de vMatrix y el reproductor VLC y el reproductor VLC está seleccionado para reproducir el video, entonces estás listo para "guardar".
<br><br>
Nota:Dado que el contenido multimedia no se procesa en el lado del host, la utilización de esta función presenta un inconveniente: el administrador no puede ver el contenido multimedia dentro del reproductor VLC a través del monitoreo en el lado del host.
</p>

<h3>Alternativa</h3>
<p style="font-size:13px;">
  Utilice el paquete de códec K-lite con Media Player Classic, que respalda el procesamiento de host con aceleración de hardware
  <br><br>
  Cuando se usa MPC (reproductor multimedia clásico) para reproducir videos en clientes cero, aunque el trabajo de renderización aún lo realiza la CPU host y el consumo es mayor que con el reproductor VLC, como MPC admite la decodificación de hardware, el consumo de CPU del host se reduce considerablemente , hasta tan solo 1/2 de usar otros jugadores sin aceleración de hardware.
  <br><br>
  Beblow es el enlace de descarga y los pasos de configuración:
  <br><br>
  1. Descargue el paquete de códec estándar K-lite en http://www.codecguide.com . El Media Player Classic está incluido.
  <br><br>
  2. instale el paquete K-lite en el host. Media Player Classic está integrado para que no tenga que instalarlo por separado.
  <br><br>
  3. Abra "Codec Tweak Tool" en el menú "Inicio" -> "K-Lite Codec Pack" o "Herramientas" en el archivo de instalación.
  <br><br>
  4. Haga clic en "DirectShow (x86)" si su sistema Windows es de 32 bits, o "DirectShow (x64) si es de 64 bits.
  <br><br>
  <img class="img-fluid" src="img/consejos/1.jpg" alt="">
  <br><br>
  5. En la siguiente ventana emergente, seleccione "Video: LAV Video Decoder" y aplique.
  <br><br>
  <img class="img-fluid" src="img/consejos/2.jpg" alt="">
  <br><br>
  Después de aplicarlo, "Decodificador de video LAV" pasa a "FILTROS DESACTIVADOS" como se muestra a continuación:
  <br><br>
  <img class="img-fluid" src="img/consejos/3.jpg" alt="">
  <br><br>
  6. Abra el Media Player Classic, vaya a "Ver" - "Opciones" - "Reproducir" - "Salida", seleccione la opción
  "DirectShow Video". Y todas las configuartiones se completan.
  <br><br>
  <img class="img-fluid" src="img/consejos/4.jpg" alt="">
</p>
</div>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#con-dos" aria-expanded="false" aria-controls="collapseExample">
Consejo 02: utilice el almacenamiento conectado a la red (NAS) como almacenamiento primario para los datos del usuario
</p>
<div class="collapse" id="con-dos">
<p style="font-size:13px;">
  Acerca de NAS
  NAS: Network-Attached Storage es un servidor de almacenamiento de datos informáticos a nivel de archivos conectado a una red de computadoras que brinda acceso a datos a un grupo heterogéneo de clientes.

  NAS (Network-Attached Storage) se puede integrar en la solución de cómputo compartido de vCloudPoint para asignar almacenamiento privado para cada usuario asignado de cero clientes.

  Instrucciones de instalación y usuario de Synology Network Storage

  Notas:

  Esta guía de instalación solo se aplica al modelo NAS DS216j utilizado como disco privado de los usuarios. Elija el plan de almacenamiento de red apropiado según sus necesidades reales.
  Utilice LAN de Gigabit o superior para garantizar el ancho de banda de la red NAS.
  Utilice LAN con DHCP que pueda asignar IP automáticamente.
  Para usar las otras funciones de NAS de Sysnology, consulte el sitio web oficial de Synology: https://www.synology.com
  Modelos de hardware:

  Servidor NAS: Synology DiskStation DS216j

  Disco duro: Western Digital NAS Red Disk 4TB * 2

  Configuración de hardware:

  Instale el disco duro en el NAS y asegúrelo con tornillos.
  Conecte el NAS al conmutador con un cable Ethernet.
  Conecte el NAS a la corriente y finalice la configuración.
   Instalación por primera vez:

  Encienda el dispositivo NAS y espere de 1 a 2 minutos.
  Abra el navegador con cualquier computadora en la misma LAN, vea http://find.synology.com o la dirección IP del dispositivo NAS para ingresar a la página del Asistente Web.
  Después de ingresar a la página Web Assistant, haga clic en  Instalar ahora y confirme el modo de disco duro (seleccione el sistema predeterminado) para descargar el sistema Synology DSM de Internet, siga las instrucciones para completar la instalación.
  Después de instalar el sistema, cree la cuenta de administrador y personalice la actualización y el mantenimiento de DSM; deje la configuración QuickConnect como predeterminada.
  Una vez completada la instalación inicial, ingrese al sistema, esta solución de almacenamiento se usa solo como un disco privado para usuarios de vCloudPoint, otros complementos no son necesarios para la instalación.
   Configuración rápida del sistema NAS:

  Abra el  panel de control ->  compartir archivos  en  carpetas compartidas y edite  casas
  Seleccione: ( Ocultar esta carpeta compartida en "Mis sitios de red"; Ocultar subcarpetas y archivos de usuarios sin permiso; Habilitar papelera de reciclaje ) Cancelar: ( Restringir el acceso solo al administrador )
  Confirmar  la configuración de las casas
  Aplique los mismos pasos para el resto de las Carpetas Compartidas .
  Abrir conectividad->  Red  ->  Red de Interfaz , seleccione  LAN y edición:
  I Pv4 ->  Usar la configuración manual , configurar la dirección IP estática para el dispositivo NAS;
  IPv6 -> Seleccione Off para desactivar el IPv6 , a continuación, confirmar la
  Abra Conectividad->  red-  >  control de tráfico , seleccione la esquina superior derecha de la  LAN ->   Haga clic en Editar -> seleccione  Todos los puertos , configure los  ajustes de ancho de banda . El ancho de banda de transmisión NAS LAN predeterminado es de 1 Gbps. Pero recomendamos configurarlo a 20000KB / s, el más grande 40000KB / s. Si no se establece la limitación del ancho de banda, los usuarios utilizarán todo el ancho de banda mientras transfieren archivos.
  Abra Sistema->   estilo de tema , establezca el título y el tema de la página de destino de la página según sea necesario.
  Abrir Sistema->   Artículos de ferretería y Poder ... se puede personalizar recuperación de energía , control de Bip, el modo de velocidad del ventilador, Led control de brillo.
  Abrir Archivo sharing->  Usuario ->  Avanzado :  Contraseña Configuración->   aplica la regla de la contraseña, desplácese hacia abajo para usuario Inicio ->   Habilitar servicios para el hogar del usuario  y  papelera de reciclaje .
  Abrir uso compartido de archivos ->   Cuentas de usuario ,  crear  usuarios .
  Ingrese el nombre de usuario y la contraseña (recomendamos que la contraseña de NAS sea diferente de la contraseña de usuario de vCloudPoint), Siguiente paso .
  Grupo predeterminado del sistema : dejar como predeterminado, Siguiente paso .
  Asignar permisos de carpetas compartidas : dejar como predeterminado, Siguiente paso .
  Configuración de cuotas de usuario : establézcala de acuerdo con el uso real de cada usuario y puede establecer como límite o ilimitado, Siguiente paso .
  Asignar permiso de aplicación : dejar como predeterminado, Siguiente paso .
  Límite de velocidad del usuario : dejar como predeterminado Siguiente paso .
  Verifique la nueva configuración del usuario, confirme y luego Aplicar .
  Seleccione el usuario que acaba de crear, seleccione usuario -> Crear -> Copiar usuario, simplemente ingrese el nombre de usuario y la contraseña, y copie toda la configuración del usuario.
  Instalación de NAS completa.
   Conéctese al dispositivo NAS en un cliente cero:

  Abra el Explorador de archivos, ingrese la dirección IP del dispositivo NAS: \\ xxx.xxx.xxx.xxx.
  Ingrese el nombre de usuario y la contraseña NAS asignados, presione OK.
  Haga clic con el botón derecho en la carpeta de inicio  , seleccione Map Network Drive , haga clic en Finish .
</p>
</div>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#con-tres" aria-expanded="false" aria-controls="collapseExample">
Consejo 03: Restrinja a los usuarios la ejecución de aplicaciones específicas
</p>
<div class="collapse" id="con-tres">
  <h3>Restricción de software para usuarios de cero clientes mediante la política de grupo de AppLocker</h3>
  <p style="font-size:13px;">
  <br><br>
  <b>Descripción:</b>
  <br>
  Política de grupo de AppLocker para el sistema de Windows. Las aplicaciones se pueden utilizar para restringir la ejecución e instalación
  de grupos de usuarios o usuarios.
  <br><br>
  Normalmente, solo los administradores tienen permiso para instalar programas. Pero el software verde y
  otro paquete de software no necesitan necesariamente el permiso de los administradores para su instalación.
  Por lo tanto, el uso de la política del Grupo AppLocker puede limitar directamente al Usuario el acceso y la
  instalación de todos los programas.
  <br><br>
  <b>Consejos:</b>

  La Política de grupo de AppLocker debe usarse junto con el Control de cuentas de usuario (UAC). Consulte la Guía de control de cuentas de usuario para activar UAC.
  <br>
  Antes de configurar AppLocker, estandarice la ruta de instalación del programa, asegúrese de instalar los programas necesarios en la ruta C: \ Archivos de programa
  o C: \ Archivos de programa (x86). Como la carpeta Archivos de programa es un tipo de archivo de sistema, que requiere permiso del administrador para realizar
  cambios.
  <br>
  Sistemas operativos recomendados: Windows 7 (Ultimate, Enterprise), Windows 8.1 Enterprise, Windows 10 (Professional,
  Enterprise), Server 2008R2 Standard, Datacenter, Server 2012R2 (Standard, Datacenter), Server 2016 (Standard, Datacenter).
  <br><br>
  <b>Pasos rápidos de configuración:</b>
  <br><br>
  Ingrese el servicio, establezca el tipo de inicio de identidad de la aplicación en automático
  <br>
  Introduzca el Editor de directivas de grupo local àAppLocker
  <br>
  Reglas ejecutablesà Las reglas del instalador de Windows y las especificaciones de script crean reglas predeterminadas
  <br>
  AppLocker abre reglas de configuración
  <br>
  Reinicie el host, luego la configuración de AppLocker tendrá efecto.
  <br>
</p>
</div>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#con-cuatro" aria-expanded="false" aria-controls="collapseExample">
Consejo 04: Restringir usuarios para modificar archivos y configuraciones del sistema (UAC)
</p>
<div class="collapse" id="con-cuatro">
  <h3>Restringir cero usuarios de cliente para modificar archivos de sistema y configuración mediante el control de cuentas de usuario (UAC)</h3>
  <p style="font-size:13px;">
  <br><br>
  <b>Descripción:</b>
  <br>
  Control de cuentas de usuario (UAC) es un nuevo conjunto de tecnologías de infraestructura en Windows Vista (y más adelante en los sistemas
  operativos de Microsoft) que ayuda a evitar que los programas maliciosos dañen su sistema y ayuda a las organizaciones a implementar
  plataformas más fáciles de administrar.
  <br><br>
  Con UAC, las aplicaciones y tareas siempre se ejecutan en el contexto de seguridad de una cuenta de administrador no
  administrador, excepto cuando un administrador otorga específicamente acceso de nivel de administrador al sistema.
  UAC evitará la instalación automática de aplicaciones no autorizadas para evitar cambios inadvertidos en la configuración
  del sistema. Esto puede limitar efectivamente al usuario cero del cliente para modificar el sistema.

  <b>Consejos:</b>
  <br><br>
  Los administradores deben usar una contraseña compleja.
  <br>
  No agregue cero usuarios de clientes al grupo de administradores ya que las restricciones de UAC no se aplican a los administradores.
  <br>
  Después de habilitar el UAC, cuando un usuario normal intenta abrir una carpeta que necesita permiso de administrador, se le pedirá que
  ingrese la contraseña de administrador. Se sugiere que los administradores de TI no concedan a un usuario normal con acceso de nivel
  de administrador para acceder temporalmente a una carpeta restringida. Porque una vez hecho, el usuario normal tendrá acceso permanente a esta carpeta.
  <br>
  Se recomienda que UAC se use con la restricción de acceso a la unidad C. Para obtener más detalles, consulte el documento de Restringir el acceso del
  usuario a la unidad C.
  <br>
  Nota: UAC está habilitado por defecto en ciertos sistemas de Windows.
  <br><br>
  <b>Habilitando UAC:</b>
  <br><br>
  1.- Abra el panel de control, haga clic en Cuenta de usuario y Seguridad familiar.
  <br>
  2.- Haga clic en Cuenta de usuario.
  <br>
  3.- Haga clic en Cambiar configuración de control de cuenta de usuario.
  <br>
  4.- Ajuste el nivel al tercer nivel o al nivel más alto y haga clic en Aceptar.
  <br>
  5.- La configuración de UAC está completa; reinicia el host.
  <br>
</p>
</div>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#con-cinco" aria-expanded="false" aria-controls="collapseExample">
Consejo 05: Restringir usuarios para acceder a la unidad C
</p>
<div class="collapse" id="con-cinco">
  <h3>Restrinja el acceso de los usuarios a la unidad C a través de la política de grupo</h3>
  <p style="font-size:13px;">
  <b>Nota:</b>
  <br><br>
  Puede configurar la Política de grupo para evitar el acceso a la unidad C a través del Administrador de archivos del sistema,
  pero no puede restringir el acceso a través del administrador de archivos de terceros, por ejemplo, el software de descompresión.
  <br>
  Después de restringir el acceso a la unidad C por la directiva de grupo, todos los usuarios, incluida la cuenta de administrador,
  no pueden acceder a la unidad C. Por lo tanto, antes de configurar la política de grupo, deberá completar todas las instalaciones y
  configuraciones en la unidad C.
  <br>
  Aunque la ruta del archivo de escritorio predeterminado está en la unidad C. (C \ Users \ username \ Desktop), después de
  la configuración de restricción, los usuarios aún pueden crear, descargar o arrastrar archivos al escritorio, pero la función
  "pegar" en el escritorio está deshabilitada. Si desea volver a visitar la unidad C, simplemente modifique la configuración de
  la Política de grupo relacionada de nuevo a la predeterminada.
  <br>
  Cuando configura la restricción de acceso a la unidad C, se sugiere que tenga habilitado el UAC; de lo contrario, los usuarios
  pueden modificar la Política de grupo para volver a obtener acceso a la unidad C. Para obtener más detalles sobre UAC, consulte
  la guía "Control de cuentas de usuario".
  <br><br>
  <b>1.- Pasos de configuración: inicie </b>
  <br>
  sesión en el host con la cuenta de administrador, ejecute "gpedit.msc" para abrir el Editor de políticas de grupo.
  <br>
  2.- Configuración del equipo >> Configuración del usuario >> Plantilla administrativa >> Componentes de Windows >> Explorador de Windows >>
  Impedir el acceso a las unidades desde Mi PC >> Editar la configuración de la política.
  <br>
  Active esta configuración y seleccione "Restringir unidad C solamente", haga clic en "Aceptar".
</p>
</div>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#con-seis" aria-expanded="false" aria-controls="collapseExample">
Consejo 06: Limite el uso del disco del usuario
</p>
<div class="collapse" id="con-seis">
  <h3>Limite el uso del disco del usuario configurando cuotas de disco</h3>
<p style="font-size:13px;">
  <b>Descripción:</b>
  <br>
  las cuotas de disco se aplican a usuarios específicos y limitan la cantidad de espacio en disco que el usuario puede usar en un volumen particular.
  <br><br>
  <b>Pasos de configuración rápida:</b>
  <br>
  1) Introduzca el editor de directivas de grupo local, habilite las cuotas de disco ...
  <br>
  2) Establezca cuotas para los usuarios en las propiedades de la partición del disco.
  <br>
  3) Reinicia el host.
  <br><br>
  <b>Pasos de configuración detallados:</b>
  <br>
  1) Inicie sesión en el host con una cuenta de administrador, ejecute gpedit.msc para abrir el Editor de políticas de grupo.
  <br><br>
  2) Configuración del equipo-> Plantilla administrativa-> Sistema-> Cuotas de disco-> haga doble clic en Habilitar cuotas de disco
  para abrir la configuración de Cuota de disco.
  <br><br>
  3) Seleccione la opción Habilitar, haga clic en Aceptar para guardar la configuración.
  <br><br>
  4) Abra Mi PC, seleccione la partición de disco que desea limitar el uso. En esta guía, tomamos Private Disk (E :)
  como ejemplo. Haga clic derecho y seleccione Propiedades.
  <br><br>
  5) Seleccione la opción Cuota, marque Habilitar administración de cuotas y Denegar espacio en el disco para los usuarios que
  superen el límite de cuota, seleccione e ingrese el mismo número para Limitar el espacio en disco y el nivel de advertencia.
  <br><br>
  Nota: Esta operación solo se aplica al usuario recién creado, además deberá realizar algunas configuraciones para los usuarios creados.
  <br><br>
  Se seleccionará la opción de Denegar espacio en disco a los usuarios que excedan el límite de cuota; de lo contrario, solo se mostrará
  un recordatorio, pero no se tomarán restricciones cuando un usuario exceda el espacio disponible.
  <br><br>
  <b>Si no ha creado usuarios, puede omitir los siguientes pasos. Si ha creado usuarios, aún deberá configurar Entradas de cuota.</b>
  <br><br>
  6) Haga clic en la opción Entradas de cuota ... de la página de cuota del paso anterior.
  <br><br>
  7) Seleccione Cuota en la barra de tareas para crear una nueva entrada de cuota. Haga clic en Avanzar.
  <br><br>
  8) Haga clic en Buscar ahora, busque todos los usuarios locales creados, seleccione y haga clic en Aceptar para guardar.
  <br><br>
  9) Confirme los usuarios seleccionados y haga clic en Aceptar.
  <br><br>
  10) Aparecerán ventanas de Entrada de cuota. Ingrese el número de espacio en disco y el nivel de calentamiento, luego haga clic en Aceptar.
  <br><br>
  11) Inicie sesión con un usuario creado, desde File Explorer, verá que el tamaño de la partición del disco está limitado al número que acaba de configurar.
</p>
</div>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#con-siete" aria-expanded="false" aria-controls="collapseExample">
Sugerencia 07: evitar que los usuarios cierren el host en la ventana 7 o XP
</p>
<div class="collapse" id="con-siete">
<p style="font-size:13px;">
  En sistemas con Windows 8 o posterior, los usuarios de escritorio remoto no pueden apagar el host. Pero para Windows 7 y sistemas anteriores,
  la opción de apagado está disponible para los usuarios de escritorio remoto. A continuación se muestra la guía para evitar que los usuarios de
  cero clientes cierren el host.
  <br><br>
  1) en el host, abra la consola de configuración de seguridad a través del Panel de control -> Herramientas de administración ->
  Configuración de seguridad local -> Configuración de seguridad -> Local -> Asignación correcta del usuario -> Sistema de operación de apagado
  <br><br>
  2) eliminar usuarios o grupos que no permiten apagar el host. cómo evitar que el usuario de la terminal cierre el host
  <br><br>
  <img class="img-fluid" style="width:600px;" src="img/consejos/5.png" alt="">
</p>
</div>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#con-ocho" aria-expanded="false" aria-controls="collapseExample">
Consejo 08: Permitir que los usuarios de Zero Client cierren el host compartido
</p>
<div class="collapse" id="con-ocho">

  <h3>Permitir que los usuarios de Zero Client cierren el host compartido a través de la directiva de grupo</h3>
<p style="font-size:13px;">
  <b>Descripción:</b>
  mediante la configuración predeterminada de la Política de grupo de Windows, los usuarios de cero clientes no
  pueden reiniciar / apagar el host compartido. Pero en algunos casos, es posible que desee permitir que algunos
  usuarios de cero clientes cierren el host compartido cuando el administrador de TI está ausente. En este caso,
  puede configurar la Política de grupo para permitir que algunos usuarios de clientes cero cierren el servidor compartido.
  <br><br>
  <b>Notas: </b>
  <br><br><br>
  El administrador de TI debe recordar a los usuarios del cliente cero que confirmen si todos los otros usuarios en el mismo host
  se firman de forma segura antes de la operación de apagado / reinicio. Los usuarios de cero clientes pueden verificar el estado
  de conexión de todos los usuarios en el sistema host en la pestaña Usuario del Administrador de tareas (teclas de método abreviado: Ctrl + Shift + Esc).
  <br><br><br>
  Para evitar la pérdida de datos o el apagado / reinicio fallido, los usuarios de cero clientes deberán guardar archivos, cerrar
  aplicaciones y luego cerrar sesión en el sistema antes de apagar el dispositivo cliente cero para finalizar el trabajo del día.
  <b>Pasos de configuración:</b>
  <br><br><br>
  1.- Inicie sesión en el sistema con una cuenta de administrador, abra el <b>panel de control.</b>
  <br><br>
  2.- Abrá <b>herramientas administrativas .</b>
  <br><br>
  3.- Abrir  <b>política de seguridad local.</b>
  <br><br>
  4.- Haga clic en  <b>Configuración de seguridad</b> -> <b>Política local </b> ->  <b>Asignación de derechos de usuario</b> , seleccione  <b>cerrar el sistema</b> .
  <br><br>
  5.- Haga clic con el <b>Sistema,</b> establecidos  <b>en Propiedades</b> , haga clic en  <b>Agregar usuarios o grupo ... .</b>
  <br><br>
  6.- Cambiar el <b>tipo de objeto.</b>
  <br><br>
  7.- Seleccione  <b>Grupos</b> , confirme  <b>OK.</b> Nota: si solo desea que algunos usuarios específicos tengan permiso, deberá seleccionar la opción de  <b>Usuarios</b> .
  <br><br>
  8.- Seleccione  <b>Avance</b> ... .
  <br><br>
  9.- Haga clic en  <b>Buscar ahora</b> , seleccione el grupo de  <b>vMatrixServerRemoteUsers</b>, confirme OK. Nota: si solo desea que algunos usuarios
  específicos en lugar de todos los usuarios tengan el permiso y hayan seleccionado la opción de  <b>usuarios</b> del paso anterior, se le pedirá que seleccione
  usuarios específicos. aquí.
  <br><br>
  10.- Confirme que el grupo o los usuarios están seleccionados, haga  <b>clic</b> en  <b>Aceptar</b> .
  <br><br>
  11.- Confirme que ha agregado el grupo <b>vMatrixServerRemoteUser </b> o usuarios.
  <br><br>
  12.- Todas las configuraciones están hechas.
</p>
</div>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#con-nueve" aria-expanded="false" aria-controls="collapseExample">
Consejo 09: Desactive el requisito de complejidad de la contraseña en los sistemas de servidor de Windows
</p>
<div class="collapse" id="con-nueve">
<p style="font-size:13px;">
  <b>Deshabilite el requisito de complejidad de la contraseña en los sistemas de servidor de Windows a través de la política de grupo</b>
  <br><br>
  <b>Descripción:</b>
  <br><br>
  De forma predeterminada, Microsoft Windows Server System obliga a los usuarios a usar contraseñas seguras para mayor
  seguridad. Pero puede deshabilitar la complejidad de la contraseña a través de la política de grupo a través de la política
  de grupo para evitar el problema de utilizar una contraseña compleja para cero usuarios del cliente.
  <br><br>
  <b>Ambiente para esta guía:</b>
  <br><br>
  Windows Server 2012 R2
  <br><br>
  1, haga clic en el menú "Inicio" en la esquina inferior derecha de su escritorio, y luego vaya a "Ejecutar", <b>ingrese " secpol.msc "</b> (sin las comillas).
  Luego aparece el cuadro de configuración de la Política de seguridad local en la Política de grupo.
  <br><br>
  <img class="img-fluid" src="img/consejos/6.jpg" alt="">
  <br><br>
  2, pulse "Política de cuenta" en el panel derecho.
  <br><br>
  <img style="width:500px;" class="img-fluid" src="img/consejos/7.jpg" alt="">
  <br><br>
  3, pulse "Política de contraseñas".
  <br><br>
  <img style="width:600px;" class="img-fluid" src="img/consejos/8.jpg" alt="">
  <br><br>
  4, Buscar en la casilla "La contraseña debe cumplir los requisitos de complejidad", y luego haga doble clic para cambiar la configuración.
  <br><br>
  <img style="width:600px;" class="img-fluid" src="img/consejos/9.jpg" alt="">
  <br><br>
  5, en la ventana emergente, seleccione "Desactivar" y haga clic en "Aceptar" para aplicar.
  <br><br><img class="img-fluid" src="img/consejos/10.jpg" alt="">
  <br><br>
  6, ahora puedes establecer nuevas contraseñas simples. Si es necesario, también puede desactivar la contraseña, la antigüedad y el historial en la misma casilla.
</p>
</div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-body">
              <h3 style="margin-bottom:20px; margin-top:-5px;" class="card-title">Para un mejor uso de vCloudPoint</h3><hr>
              <p style="font-size:14px;" class="card-text">
              Estas guías muestran posibles enfoques para extender la funcionalidad de la solución
              informática compartida de vCloudPoint únicamente. UAC, la Política de grupo y otros componentes
              de Windows pertenecen a Microsoft. El otro software o hardware mencionado aquí pertenece a sus
              respectivos dueños. Para la aplicación real de los productos de terceros, consulte los documentos
              relacionados por sus propietarios.
              </p>
          </div>
        </div>
      </div>
    <div class="col-md-1"></div>
  </div>
</div>
  <div class="container">
  <div class="row">
    <div class="col-md-1"></div>
      <div class="col-md-6">
        <h3>Conocimientos base</h3>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#b-uno" aria-expanded="false" aria-controls="collapseExample">
  La configuración de un cliente cero no contribuye al rendimiento del escritorio
</p>
<div class="collapse" id="b-uno">
<p style="font-size:13px;">
  La mayoría de los clientes piensan erróneamente que un cliente cero debe ser, como los clientes ligeros, configurado en su hardware interno como CPU, flash, chipset de red, para otorgar un alto rendimiento.
  <br><br>
  A diferencia de los clientes ligeros x86 que normalmente se requieren para el uso independiente y tienen sistema local y software. Los clientes cero no ofrecen nada localmente, solo permiten a los usuarios conectarse a un escritorio remoto. En otras palabras, el hardware residente de un cliente cero no actúa en trabajos de procesamiento local como en clientes ligeros, sino solo para inicializar una conversación con la red, iniciar procesos de protocolo de red y mostrar resultados de escritorio. Por lo tanto, la configuración de un cliente cero no contribuye a su rendimiento. Incluso un cliente cero con una configuración potente como PC Pro no puede garantizar un buen rendimiento.
  <br><br>
  Entonces, ¿qué ayuda con un buen rendimiento a un cliente cero?
  <br><br>
  Hay algunos aspectos
  <br><br>
  Computadora anfitriona bien configurada, que incluye suficientes núcleos y frecuencia de CPU, IOPS y consumo de discos, tamaño y frecuencia de memoria. (La tecnología multi-threading de Intel también ayuda a aumentar la capacidad de la CPU y la serie i con aceleración de hardware funciona mejor que la serie Xeon del mismo nivel. Los discos en la configuración RAID aumentan la redundancia y el rendimiento. El procesamiento intensivo de gráficos requiere suficiente memoria. el host solo ayuda en la reproducción de video).
  <br>
  Red de baja latencia y gran ancho de banda: recomiende conexiones de red de área local proporcionadas por redes estándar de 100/1000 Mb / seg (es decir, Ethernet) entre las computadoras host y los clientes cero.
  <br>
  Protocolo de visualización remota de alta eficiencia: ofrece sesiones de alta resolución, comunicación remota de secuencias multimedia, compresión dinámica de objetos, redirección USB, asignación de unidades y mucho más.
  <br><br>
  De los 3 aspectos anteriores, solo el protocolo de visualización está determinado por los clientes cero.
  El protocolo de visualización determina dos medidas críticas: experiencia y uso de recursos. El punto de fricción
  para muchas organizaciones será varios niveles de soporte multimedia. Independientemente del estado de la implementación,
  cualquier organización puede tener problemas con el soporte multimedia. Esto es cierto no solo en las implementaciones
  grandes que empujan los límites de ancho de banda, incluso las instalaciones más pequeñas pueden consumir suficiente
  ancho de banda para superar los límites del protocolo de visualización sin un cuello de botella en el cable.
  Los clientes cero de vCloudPoint utilizaron nuestro innovador DDP (Dynamic Desktop Protocol) para la visualización de
  escritorio remota. Este protocolo está diseñado específicamente para clientes cero y está diseñado para hacer un uso
  eficiente del ancho de banda de la red y de los recursos de host.
</p>
</div>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#b-dos" aria-expanded="false" aria-controls="collapseExample">
La red tiene un gran impacto en el rendimiento cero del cliente
</p>
<div class="collapse" id="b-dos">
<p style="font-size:13px;">
Todos sabemos que los clientes cero están libres de requisitos de CPU, memoria, procesador y hardware. Esto significa que otro factor importante juega un papel en la transmisión de la información del servidor al cliente. Esa es la red. Y en el caso de clientes cero, este ancho de banda de red debe ser realmente amplio y suficiente para la transferencia de información sin interrupciones.
<br><br>
Comencemos con un ejemplo. El cliente cero S100 usa Ethernet como la conexión de red. La implementación hasta el momento ha registrado el reconocimiento de su rendimiento apreciable, que se logra con el cable de red de categoría 5 o 6 para conectarse a la red Ethernet. La siguiente pregunta es:
<br><br>
¿Qué es un cable Categoría-5 o Categoría-6?
<br><br>
Por lo general, los cables de cobre de alta calidad se utilizan para el cableado Cat-5/6. Están retorcidos en 4 pares que se extienden a lo largo de una cubierta exterior. Este diseño del cable lo hace inmune a las otras interferencias de señal, lo que significa una mejor transmisión de la señal de datos a través del cable.
<br><br>
La mayoría de las veces, el cableado deficiente es responsable de la transmisión de red defectuosa. Esto a menudo conduce a la prueba innecesaria de los otros equipos. Hay unos estándares muy altos etiquetados con el cableado de categoría 5 y 6, que también se deben seguir durante la instalación. Esto garantiza un alto rendimiento en la red.
<br><br>
Recomendación con nosotros:
<br><br>
Para entornos multimedia intensivos, especialmente aquellos con reproducción simultánea de múltiples videos, recomendamos una red estándar de 100/1000 Mbps entre la computadora host y los clientes cero, para garantizar una experiencia de alto nivel sin problemas; como se muestra en las figuras: (haga clic para ver imágenes grandes)
<br><br>
vCloudPoint-Network-Structure

vCloudPoint-Network-Structure
<br><br>
Los videos consumen mayor ancho de banda, por ejemplo, cuando los videos del servidor se reproducen en cero clientes de vCloudPoint, cada archivo de video 480p consume hasta 13 Mpbs del ancho de banda de la red, un video de 720p puede tomar hasta 15 Mpbs de ancho de banda y un video 1080p puede tomar hasta 17 Mpbs de ancho de banda directamente.
<br><br>
La mayoría de las redes de área amplia (WAN) tienen una latencia excesiva y un ancho de banda inferior. Esto se compromete con la experiencia rica en PC a través de la red.
<br><br>
La recomendación ideal de vCloudPoint es una red de área local de bajo ancho de banda de baja latencia, que es el secreto detrás del excelente rendimiento de los dispositivos cliente cero.
</p>
</div>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#b-tres" aria-expanded="false" aria-controls="collapseExample">
Obtener el flaco en clientes cero vs. clientes delgados
</p>
<div class="collapse" id="b-tres">
<p style="font-size:13px;">
<b>  Los clientes delgados solían ser los más delgados, pero ahora los clientes cero les están echando
  a correr por su dinero casi sin requisitos de almacenamiento o configuración.
</b>
<br><br>
  Cero clientes son similares a los clientes ligeros en su propósito, accediendo a una computadora de escritorio en un centro de datos, pero cero clientes requieren mucha menos configuración.
<br><br>
  Los clientes cero tienden a ser dispositivos pequeños y simples con un conjunto de características estándar que admiten la mayoría de los usuarios. También tienden a estar dedicados a un producto de escritorio de un centro de datos y a un protocolo de visualización remota. Por lo general, la configuración es simple con un par de docenas de configuraciones como máximo, en comparación con las miles de configuraciones en un sistema operativo de escritorio. Cero clientes cargan su configuración simple desde la red cada vez que están encendidos, lo que significa que los clientes cero en un sitio serán todos iguales.
<br><br>
  Los clientes cero admiten el acceso a una variedad de tipos de escritorio, servicios de terminal, una infraestructura de escritorio virtual o estaciones de trabajo en rack o montadas en rack dedicadas.
<br><br>
<h3>Cero clientes vs. clientes delgados</h3>
  <br><br>
  La premisa básica de un cliente cero es que el dispositivo en el escritorio del usuario no tenga ninguna configuración persistente. En cambio, aprende a proporcionar acceso al escritorio desde la red cada vez que se inicia. Esto proporciona muchos beneficios operativos porque los dispositivos de cero clientes nunca son únicos. Esto contrasta con un cliente ligero, que puede tener aplicaciones locales instaladas y mantendrá su configuración en el almacenamiento persistente en el dispositivo.
<br><br>
  Los thin clients originales se parecían mucho a lo que ahora llamamos clientes cero. Eran dispositivos simples que daban acceso a una computadora de escritorio en un centro de datos. Dos factores llevaron a la evolución de clientes delgados en dispositivos más gruesos. El primero fue la entrada de los fabricantes de PC en el mercado de clientes livianos, y el segundo fue la necesidad de evitar las limitaciones de mostrar de forma remota un escritorio de Windows.
<br><br>
  <b>Una breve historia de clientes ligeros</b>
<br><br>
  Los clientes delgados se convirtieron en una clase principal de productos poco después de que Microsoft presentara Windows Terminal Server y Citrix lanzara MetaFrame, ambos en 1998. Para entrar en este mercado, los fabricantes de PC redujeron sus plataformas de hardware de escritorio. Reutilizaron sus herramientas de administración de PC, reutilizando la mayor cantidad de tecnología posible de sus negocios de PC existentes. Esto significaba que una configuración de Windows o Linux bastante personalizada podría convertirse en un cliente ligero. Pero incluso una versión de Linux personalizada generalmente tiene una configuración local y un poco de almacenamiento. Como resultado, estos clientes ligeros no eran tan delgados, y sus herramientas de gestión podían ser relativamente complejas.
<br><br>
  La otra motivación para engrosar a los clientes fue la necesidad de manejar medios enriquecidos, como el video. Los primeros protocolos de visualización remota no produjeron excelentes resultados, por lo que aparecieron tecnologías para el procesamiento de videos de clientes ligeros. Estos utilizan reproductores multimedia en el cliente ligero en lugar de en el escritorio, transfiriendo la transmisión de video comprimido a través de la red. Ahora el thin client necesitaba códecs de video así como también un sistema operativo local.
<br><br>
  Con el tiempo, se agregaron características opcionales para la redirección USB, un navegador web local, agentes de integración de voz sobre IP y compatibilidad con pantallas de múltiples monitores. Cada característica adicional aumentó la configuración y la complejidad del thin client. Después de unos años, los clientes ligeros se convirtieron en PC pequeñas. Algunos incluso agregaron ranuras para tarjetas PCI o PC.
<br><br>
  Estos clientes delgados y más gruesos se acercan bastante a una PC completa en términos de capacidades y complejidad. En lugar de simplificar la administración, los clientes ligeros obligaron a los administradores de TI a administrar el dispositivo en el escritorio del usuario y en el centro de datos. Esto obviamente no es lo que teníamos en mente. Cero clientes son un retorno a los dispositivos más simples en los escritorios de los usuarios con una administración más simple.
<br><br>
<b>Beneficios operacionales y de seguridad de clientes cero vs. clientes ligeros</b>
<br><br>
  Cualquier trabajador puede usar cualquier cliente cero porque el cliente cero no almacena nada único. Esto les da a los trabajadores la flexibilidad de acceder a sus escritorios desde cualquier cliente cero en cualquier parte de la organización, lo que reduce la necesidad de que los empleados lleven computadoras portátiles entre sucursales o escritorios compartidos. Una organización puede simplemente proporcionar escritorios con cero clientes, con toda la exclusividad almacenada en el escritorio del centro de datos.
<br><br>
  Otro beneficio es que un nuevo cliente cero se puede enviar directamente al usuario. No hay necesidad de soporte de escritorio o preconfiguración. Todas las necesidades del cliente cero son una conexión de red y una fuente de alimentación. Para una organización con muchas sucursales, esta simplicidad ahorra tiempo y facilita considerablemente la configuración de una nueva contratación o la asistencia de un trabajador cuyo dispositivo está fallando. Las empresas pueden incluso mantener un cliente cero de repuesto en cada sucursal, lo que elimina las demoras en el envío.
<br><br>
  Normalmente, los clientes cero cargan su configuración desde la red, a menudo desde unos pocos archivos y los comparte cada cliente cero en un sitio. Cambiar una configuración generalmente significa cambiar estos archivos compartidos y luego reiniciar los clientes cero. La actualización de cero clientes generalmente es el mismo proceso: coloque un nuevo archivo de firmware junto con los archivos de configuración y reinicie los cero clientes. El nuevo firmware se carga automáticamente cuando se inicia el cliente cero. Esto hace que sea bastante simple mantener constantes las compilaciones de cliente cero, siempre y cuando los usuarios apaguen sus clientes cero al final del día.
<br><br>
  Una razón común para implementar escritorios basados ​​en centros de datos es contener datos para que la propiedad intelectual de una empresa no se distribuya a todos los escritorios. Como los clientes zero normalmente no tienen casi ningún almacenamiento persistente, hay muchas menos posibilidades de que permanezcan datos críticos en el dispositivo. Además, el cliente cero no ejecuta un sistema operativo de propósito general, por lo que es poco probable que se vea comprometido por un virus o intrusión de red. De hecho, esta falta de almacenamiento local es un factor decisivo para implementar clientes cero en algunos entornos altamente seguros.
<br><br>
  Los clientes cero proporcionan estos beneficios sin la intrusión de un sistema operativo local. Por ejemplo, los usuarios no desean ver cómo el dispositivo cliente maneja la llave USB que acaban de enchufar, solo a su escritorio le debe importar. Sin embargo, con los clientes ligeros, un sistema operativo local debe identificar y manejar el dispositivo USB antes de entregárselo al cliente de visualización remota y luego al escritorio del usuario. Además de ser un proceso lento, hay una mayor posibilidad de que algo en el camino falle. Los clientes cero simplemente pasan el dispositivo USB directamente al escritorio, lo que resulta en una experiencia mucho más parecida a la de una PC. Los clientes cero también tienden a mostrar una interfaz local mucho más ligera, llegando al escritorio del usuario con menos cajas de diálogo que los clientes ligeros.
<br><br>
  (Esto fue publicado por última vez en abril de 2014 por Alastair Cooke en searchvirtualdesktop.techtarget.com)
</p>
</div>
        <p style="margin-bottom:10px; font-size:13px;" class="preguntas" data-toggle="collapse" data-target="#b-cuatro" aria-expanded="false" aria-controls="collapseExample">
Cero cliente vs. computación de cliente ligero: por qué cero clientes son mejores
</p>
<div class="collapse" id="b-cuatro">
<p style="font-size:13px;">
<b>  Los clientes cero y los clientes ligeros ofrecen acceso de escritorio virtual, pero no son lo mismo
  en términos de rendimiento y escalabilidad.
</b>
<br><br>
Un cliente cero es similar a un cliente ligero, ya que ambos proporcionan acceso a escritorios virtuales, pero tienen diferencias significativas en lo que respecta al rendimiento, la usabilidad y la seguridad.
<br><br>
Un cliente cero es un dispositivo liviano que brinda al usuario acceso a un escritorio almacenado en un centro de datos. Un thin client se parece más a una PC pequeña, con pocas opciones de expansión. También puede ser un portal para un escritorio en un centro de datos, pero también puede traer mucha complejidad.
<br><br>
<b>Cero cliente contra computación de cliente ligero: Plug and play</b>
<br><br>
Los clientes cero proporcionan la experiencia más parecida a una PC para la infraestructura de escritorio virtual (VDI). Un ejemplo tiene que ver con el USB plug-and-play. Al usar una PC o Thin Client con un sistema operativo local, hay tres ciclos de conectar y usar cuando conecta un dispositivo USB:
<br><br>
Dispositivo local plug-and-play. La detección del dispositivo y la instalación del controlador para el dispositivo USB
<br>
VDI cliente superan. Sustitución del controlador del sistema operativo local por un controlador de redirección
<br>
Plug-and-play remoto (dentro del escritorio virtual). Detección del dispositivo e instalación del controlador para el
dispositivo USB
<br>
Puede tomar un tiempo, a veces minutos, desde el momento en que conecta la llave USB hasta que se muestra como un dispositivo reconocido. Por lo general, las dos primeras acciones plug-and-play no son visibles para el usuario, por lo que se rinden con frustración y eliminan la llave USB antes de que comience la tercera etapa.
<br><br>
Sin embargo, con un cliente cero, no debería haber plug-and-play local. Toda la experiencia de USB se transfiere al escritorio virtual en el centro de datos. Solo se produce el plug-and-play remoto dentro de ese escritorio, y el usuario puede verlo, por lo que el usuario sabe esperar a que termine.
<br><br>
<b>Cliente cero vs. rendimiento y manejabilidad del cliente ligero</b>
<br><br>
Los clientes cero tienden a ofrecer un alto rendimiento. Deberían estar bien optimizados para un protocolo VDI y proporcionar una excelente experiencia de usuario con desplazamiento rápido y sin problemas y la mejor reproducción de video que la red permitirá. La experiencia del usuario es el factor determinante en el éxito de VDI, por lo que es fundamental utilizar un dispositivo optimizado para el protocolo VDI. Normalmente, los clientes cero proporcionan el mejor rendimiento de visualización para el protocolo.
<br><br>
Los clientes cero tienden a ser mucho más simples de administrar, configurar y actualizar. Las imágenes de firmware cero del cliente tienen un tamaño de algunos megabytes, en comparación con los múltiples gigabytes que ocupan los sistemas operativos del cliente ligero. El proceso de actualización es mucho más rápido y menos intrusivo en un cliente cero que en un thin client, posiblemente todos los días cuando se inicia el cliente cero. Los clientes delgados necesitan ser reparados y actualizados con la misma frecuencia que el sistema operativo de escritorio que llevan, pero cero clientes no tienen un sistema operativo, por lo que necesitan menos actualizaciones.
<br><br>
Los clientes con cero tienen pocas perillas e interruptores para girar, probablemente menos de 100 elementos de configuración en total, por lo que son fáciles de administrar. A menudo, su gestión de volumen es un par de archivos de texto en una red compartida en alguna parte. Los clientes delgados tienen un sistema operativo completo para administrar, con decenas de miles de configuraciones que requieren aplicaciones de administración complejas, generalmente en servidores dedicados en múltiples sitios.
<br><br>
Un cliente cero es como una tostadora: un consumidor puede sacarlo de su empaque y hacerlo funcionar. Si el consumidor es su personal en una sucursal remota, entonces hay muchos beneficios de poder hacer el despliegue de una nueva terminal.
<br><br>
En ocasiones, los clientes ligeros necesitan compilaciones especiales o configuraciones personalizadas antes de su implementación, lo que no es ideal para una implementación rápida. La capacidad de escalar rápidamente puede ser importante cuando se trata de abrir un centro de llamadas para adaptar una campaña publicitaria o una respuesta de desastre natural.
<br><br>
Otra ventaja de cero clientes es su menor consumo de energía. Los clientes delgados tienen CPU convencionales y, a menudo, unidades de procesamiento de gráficos, pero un cliente cero generalmente tiene una CPU de bajo consumo (o ninguna), lo que reduce el consumo de energía y la generación de calor. La simplicidad de los clientes cero también crea una superficie de ataque mucho más pequeña, por lo que colocarlos en redes menos confiables no es tan preocupante. Además, colocarlos en lugares físicamente hostiles es seguro; una menor potencia y, por lo general, una refrigeración pasiva significan que es menos probable que el calor, el polvo y la vibración causen problemas de mantenimiento.
<br><br>
Cero clientes son todos iguales. Los modelos no se lanzan cada pocos meses, sino cada dos años, por lo que su flota contendrá menos modelos diferentes. Eso significa que no hay necesidad de llamadas a la mesa de ayuda para mover un dispositivo de un escritorio a otro, además de una experiencia de usuario mucho más uniforme. El inventario de cero clientes de su proveedor también tendrá menos modelos, lo que debería conducir a una mejor disponibilidad cuando necesite nuevos clientes cero.
<br><br>
(Esto fue publicado por última vez en abril de 2014 por Alastair Cooke en searchvirtualdesktop.techtarget.com)
</p>
</div>
      </div>
      <div class="col-md-4 pb-5">
        <div class="card ">
            <div class="card-body">
                <h2 class="card-title">Aprende la tecnología detrás</h2>
                <p style="font-size:14px;" class="card-text">
                  Estos artículos han sido escritos por el personal de vCloudPoint o recogidos de Internet desde una perspectiva objetiva para ayudarlo a aprender de forma fácil y correcta esta tecnología y la industria, a fin de ayudarlo a tomar una mejor decisión de compra.
                </p>
            </div>
        </div>
      </div>
    <div class="col-md-1"></div>
  </div>
</div>









<div class="row" style="background-color:#333333;">
	<div class="container">
		<div class="row" style="margin-right:15px; margin-left:15px;">
			<div class="col-md-3">
				<img class="img-fluid  pt-3 pb-3" src="img/logo vcloudpoint1.png ">
				<p class="text-justify pt-2 footer-descripcion">
					Las tecnologías vCloudPoint hacen que la informática de escritorio sea extremadamente simple y económica. Debido a la excelente
					flexibilidad y simplicidad de sus cliente cero, puede implementar, usar y administrar fácilmente escritorios para docenas
					a cientos de usuarios a una fracción de los costos.
				</p>
			</div>
		
			<div class="col-md-3">
				<p style="color:white;  font-size:20px;" class="pt-3 pb-3 text-center">Contacto</p>
				<a>
					<p class="text-center" style="font-size:13px;"><i class="far fa-envelope"></i>  ventas@vcloudpoint.com.mx</p>
				</a>
				<hr style="background-color:white;">
				<p class="text-center" style="font-size:14px;"><i class="fas fa-phone-volume"></i> 01 (55) 5615-2916</p>
			</div>

			<div class="col-md-3">
				<p style="color:white; font-family: sans-serif; font-size:20px;" class="pt-3 pb-3 text-center">Noticias recientes</p>
				<a href="18-abril.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>
						Aviso: Se solucionó el problema con la actualización del sistema de
						Windows que causaba el inicio de sesión de varios usuarios no funcionara.
					</p>
				</a>
				<p style="font-size:10px;">18 de Abril del 2019</p>
				<hr class="footer2">
				<a href="18-marzo.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>VCLOUDPOINT China fue otorgado como miembro de CEEIA y SZEEIA.</p>
				</a>
				<p style="font-size:10px;">18 de Marzo del 2019</p>
			</div>

			<div class="col-md-3">
				<br>
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:40px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="http://www.facebook.com/vCloudPointMX/"> <i class="fab fa-facebook-f"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://youtu.be/cmTRDUURWTc"><i class="fab fa-youtube"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.linkedin.com/in/vcloudpoint-mexico-86a194168/"><i class="fab fa-linkedin-in"></i></a>
					</div>
				</div>
				
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:50px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://twitter.com/vcloudmx?lang=es"><i class="fab fa-twitter"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.instagram.com/vcloudpointmx/"><i class="fab fa-instagram"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--barra-progreso-->
<script type="text/javascript">
	function animar (){
		document.getElementById("barra1").classList.toggle ("final1");
		document.getElementById("barra2").classList.toggle ("final2");
		document.getElementById("barra3").classList.toggle ("final3");

	}
	window.onload = function (){
	  animar();

	}
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?51BGgA3dFcKcI1mPpd4Un7VFblV9TgM1";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


<footer style="background-color:#262626; height: auto;">
	<div class="container">
		<div class="row">
			<p style="color:#c5c5c5; font-size:14px;" class="text-center copyright w-100">vCloudPoint 2019. Todos los derechos reservados</p>
		</div> <!--.row-->
	</div><!--.container-->
</footer>









    <script src="js/jquery.slim.js"></script>
	<script src="js/app.js"></script>
    <script src="js/menu-activo.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/menu-hover.js"></script>
    <script src="js/validar-numero.js"></script>
  </body>
</html>
