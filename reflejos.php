<!DOCTYPE html>
<html lang="en">
	<head>
		<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
			<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|PT+Sans:400,700|Roboto:400,700,900" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
			<link rel="stylesheet" href="css/styles.css">
			<link rel="stylesheet" href="css/menu.css">
			<link rel="stylesheet" href="css/footer.css">
			<link rel="stylesheet" href="css/container-inicio.css">
			<link rel="stylesheet" href="css/noticias.css">
			<link rel="stylesheet" href="css/caracteristicas.css">
			<link rel="stylesheet" href="css/divisor.css">
			<link rel="stylesheet" href="css/separador.css">
			<link rel="stylesheet" href="css/menu-activo.css">
			<link rel="stylesheet" href="css/formulario.css">
			<link rel="stylesheet" href="css/galeria.css">
			<link rel="stylesheet" href="css/imagen-hover.css">
			<link rel="stylesheet" href="css/nav-tabs.css">
			<link rel="stylesheet" href="css/productos.css">

			<link rel="icon" href="img/logo.ico">
	</head>

	<body onselectstart="return false">
		<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="height:auto;">
			<div class="container">
				<div class="col-md-3">
					<a class="navbar-brand" href="index.php">
						<img src="img/logo vcloudpoint.png" style="margin:auto; width:200px; height:auto; padding:10px;" alt=""> 
					</a>

					<button style="border:none;  background-color:#0c96cc;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span style="background-color:#0c96cc;" class="navbar-toggler-icon">
						</span>
					</button>
				</div>

				<div class="col-md-9" style="margin-left:30px;">
					<div  class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active" style="margin-right:5px;">
								<a class="nav-link home" style="padding-right:0px; padding-left:0px;" href="index.php">
									<i class="fas fa-home"></i>
									<span class="sr-only">(current)</span>
								</a>
							</li>

							<li class="nav-item" style="margin-right:5px;">
								<a class="nav-link productos" style="padding-right:0px; padding-left:0px;" href="productos.php">
									<i class="fas fa-stop"></i>
								</a>
							</li>

							<li id="m-industria" style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#industria" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-building"></i>
								</a>
								<ul class="dropdown-menu" id="industria">
									<li>
										<a href="educacion.php">
											<i class="fa fa-graduation-cap"></i>
										</a>
									</li>

									<li>
										<a href="pymes.php">
											<i class="fas fa-briefcase"></i>
										</a>
									</li>

									<li>
										<a href="servicios.php">
											<i class="fas fa-comments"></i>
										</a>
									</li>

									<li>
										<a href="espacios-publicos.php">
											<i class="fas fa-cloud"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#soporte" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" 			aria-expanded="true">
									<i class="fas fa-user-shield"></i>
								</a>
								<ul class="dropdown-menu" id="soporte">
									<li>
										<a href="instalacion.php">
											<i class="fas fa-download"></i>
										</a>
									</li>
									<li>
										<a href="preguntas-frecuentes.php">
											<i class="far fa-question-circle"></i>
										</a>
									</li>
									<li>
										<a href="complementos.php">
											<i class="fas fa-plus"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" 
								class="dropdown">
								<a href="#" data-target="#compania" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-calendar-alt"></i>
								</a>
								<ul class="dropdown-menu" id="compania">
									<li>
										<a href="nosotros.php">
											<i class="fa fa-industry"></i>
										</a>
									</li>
									<li>
										<a href="partner.php">
											<i class="fas fa-user-tie"></i>
										</a>
									</li>
									<li>
										<a href="noticias.php">
											<i class="far fa-newspaper"></i>
										</a>
									</li>
									<li>
										<a href="contacto.php">
											<i class="fa fa-phone-square"></i>
										</a>
									</li>
								</ul>
							</li>

							<li class="" style="padding-left:10px;">
								<a id="menu-casos" style="color:#666666 ; padding-right:0px; padding-left:0px;  font-size:13px; margin-top:8px;" class="nav-link casos-exito" href="casos_exito.php">
									Casos de éxito
								</a>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

  
	<div id="divisor" class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb" style="background-color:transparent;">
					<li style="margin-top:-2px!important;">
						<a style="font-size:12px; color:#8b8b8b; " href="index.php">
							<img style="width:15px; margin-top:-6px;" src="img/glyphicons/png/home.png" alt="">  Inicio 
						</a>
					</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Cliente Cero Ventajas</li>
				</ol>
			</div>
		</div>
	</div>

    <div class="container">
        <h3 style="margin-top:50px;" class="col-12 text-center">
            vCloudPoint Cliente Cero
        </h3>
        <p style="margin-top:50px; margin-bottom:50px; color:#4a4a4a;">
            Los clientes cero vCloudPoint, brindan una forma innovadora de cómputo gracias a su software propietario y patentado vMatrix, entregando la misma experiencia de una PC de escritorio tradicional a un menor costo.
	    </p>
    </div><!--.container separacion-->

    <div  style="margin-top:50px; margin-bottom:50px;" class="container">
        <div  class="row">
            <div  class="col-md-1"></div>
            <div   class="col-12 col-md-2  pt-4">
                <a id="menu"    href="productos.php"  class="boton-productos btn  btn-outline-danger">
                    <i class="iconos-botones-productos fas fa-signal"></i> 
                    Conjunto
                </a>
            </div>

            <div  class="col-12 col-md-2  pt-4">
                <a id="menu"   href="beneficios.php" class="boton-productos btn  btn-outline-danger">
                    <i class="iconos-botones-productos fas fa-certificate"></i> Beneficios
                </a>
            </div>

            <div  class="col-12 col-md-2  pt-4">
                <a id="menu"  href="reflejos.php" class="boton-productos btn  btn-outline-danger activo">
                    <i class="iconos-botones-productos far fa-star"></i> 
                    Destacados
                </a>
            </div>

            <div   class="col-12 col-md-2  pt-4">
                <a id="menu"   href="comparaciones.php" class="boton-productos btn  btn-outline-danger">
                    <i class="iconos-botones-productos fas fa-ellipsis-h"></i> Comparaciones
                </a>
            </div>

            <div  class="col-12 col-md-2  pt-4">
                <a id="menu"   href="prueba-gratuita.php" class="boton-productos btn  btn-outline-danger">
                    <i class="iconos-botones-productos far fa-hand-point-up"></i> Prueba gratis
                </a>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>


    <div class="iconos-centro">
        <i class="fas fa-rocket"></i>
        <h3 class="pt-4">
            Encienda su computadora de escritorio
        </h3>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="text-justify" style="font-size:22px; color:gray;">
                    La solución vCloudPoint proviene de una combinación de conocimiento, innovación y profundo conocimiento de las tecnologías informáticas que ninguna otra solución puede igualar.
                </p>
            </div>
        </div>
    </div>

    <div class="container" style="padding-top: 10px; padding-bottom: 10px;">
        <div class="row" style="display: flex; justify-content: center; align-items: center;">
            <div class="col-md-6">
                <h3 style="text-align:left; font-size:18px; color:black; padding-top:20px; font-family:sans-serif;">Soporte Multimedia</h3>
                <p style="font-size:14px; color:#716966; font-family:sans-serif; margin-top: 25px;">
                    Los clientes cero tradicionales básicamente utilizan tecnología mejorada RDP o un reproductor multimedia integrado para la aceleración de video, lo que resulta en restricciones de soporte de formato de medios limitado, video embebido en la web no admitido, pocos usuarios de video simultáneos por host y aún la calidad de visualización en gran medida sacrificado muy por detrás del archivo original 720 / 1080P que afirmaron soportar.
                    <br>
                    <br>
                    Impulsado por el innovador DDP (Dynamic Desktop Protocol) de vCloudPoint, los usuarios experimentarán videos reales con calidad de PC a escala de resolución de 1920 × 1080 a máxima velocidad de cuadros sin un procesamiento excesivo del lado del host o requiriendo un cliente delgado o cero con reproductor de medios y soporte de códec . El contenido de video puede ser cualquier formato presente y futuro y reproducirse a través de reproductores de medios independientes o incrustados en páginas web.
                </p>
            </div>
            <div class="col-md-6">
                <div class="primer-video">
                    <iframe class="primer-video" src="https://www.youtube.com/embed/gS7yRDPnP6I" width="100%" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>

                </div>
            </div>
        </div>
    </div>

    <div class="container" style="padding-top: 10px; padding-bottom: 10px;">
        <div class="row" style="display: flex; justify-content: center; align-items: center;">
            <div class="col-12 col-md-6 segundo-video">
                <iframe class="segundo-video" src="https://www.youtube.com/embed/cOmvKCgkYlQ" width="100%" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
            </div>
            <div style="margin-bottom:10px;" class="col-12 col-md-6">
                <h3 style="font-size:18px; text-align: left; color:black; font-family:sans-serif;">
                    Audio confiable y de alta calidad
                </h3>
                <p style="font-size:14px; color:#716966; font-family:sans-serif; margin-top: 25px;">
                    La redirección de audio de Windows utilizada por la mayoría de los clientes cero o delgados generalmente no se diseñó específicamente para el escenario de uso compartido, lo que provoca frecuentes problemas de incompatibilidad de audio o falta de fiabilidad en el escritorio remoto.
                    <br>
                    <br>
                    En lugar de utilizar el audio incorporado de Windows para escritorios remotos, los clientes cero de vCloudPoint utilizan su tecnología de audio virtual específica que permite la reproducción y grabación de audio confiable en cualquier sistema Windows, sin limitaciones ocultas o configuraciones adicionales. Para lograr aún más alta calidad y audio sincronizado, la tecnología de audio virtual de vCloudPoint reduce el retardo de transferencia a 0.1 ~ 0.2 segundos - 1/20 de eso por RDP y usa una frecuencia de muestreo de 44.1KHz que es equivalente a la calidad de CD y 48KHz que se usa en profesionales producción de música, que le proporciona la mejor experiencia de audio, sin importar si está viendo una gran película, ofreciendo servicio al cliente en un centro de llamadas o audio-enseñanza en un laboratorio de idiomas.
                </p>
            </div>
        </div>
    </div>

    <div class="container" style="padding-top: 10px; padding-bottom: 20px;">
        <div class="row" style="display: flex; justify-content: center; align-items: center;">
            <div style="margin-bottom:10x;" class="col-12 col-md-6">
                <h3 style="font-size:18px; text-align: left; color:black; font-family:sans-serif;">
                    Amplia Compatibilidad con periféricos USB
                </h3>
                <p style="font-size:14px; color:#716966; font-family:sans-serif; margin-top: 25px;">
                    Los sistemas operativos integrados de EndPoint a menudo requieren controladores de clientes personalizados, lo que reduce en gran medida la gama de periféricos que son compatibles de forma nativa y aumenta los plazos de implementación y los costos de soporte continúo. Incluso para los periféricos compatibles, todavía tienen varios problemas, como el dispositivo que el sistema no detecta a menudo si los usuarios no los conectan estrictamente según sea necesario.
                    <br>
                    <br>
                    Los Clientes Cero VcloudPoint utilizan su tecnología USB transparente, por lo que los periféricos nunca requieren controladores de clientes especializados para funcionar, solo se requiere un controlador nativo de Windows. Los Clientes Cero de vCloudPoint son compatibles con una amplia gama de dispositivos USB, incluidos dispositivos de memoria, auriculares, impresoras, scaners, pantalla táctil, lector de tarjetas y convertidores USB. Estos dispositivos USB son compatibles y confiables con todos los sistemas Windows que, en cualquier momento y sin embargo, los adjunta a los Clientes Cero.
                </p>
            </div>
            <div class="col-12 col-md-6 segundo-video">
                <iframe class="segundo-video" src="https://www.youtube.com/embed/-Wc63wSbWGM" width="100%" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true">                    
                </iframe>
            </div>
        </div>
    </div>

    <div class="container" style="padding-top:10px; padding-bottom: 10px;">
        <div class="row" style="display: flex; justify-content: center; align-items: center;">
            <div class="col-12 col-md-6 tercer-video">
                <iframe class="tercer-video" src="https://www.youtube.com/embed/l-T4QzlOHfA" width="100%" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true">
                </iframe>

            </div>
            
            <div class="col-12 col-md-6 ">
                <h3 style="font-size:18px; text-align: left; color:black; font-family:sans-serif;">
                    Seguridad del usuario
                </h3>
                <p style="font-size:14px; color:#716966; font-family:sans-serif;">
                    El software vMatrix Server supera la debilidad de privacidad de la solución de uso compartido tradicional al colocar cada sesión de usuario en una sección automática. Cada usuario de vCloudPoint tiene su espacio de trabajo privado al que otros usuarios no pueden acceder, mientras que las demás particiones de disco aún pueden configurarse como visibles o invisibles para los usuarios. Esto brinda alta seguridad a cada usuario con un espacio de trabajo único, así como una mayor eficiencia con un centro compartido.
                </p>
            </div>
        </div>
    </div>

    <div class="container" style="padding-top: 10px; padding-bottom: 10px;">
        <div class="row" style="display: flex; justify-content: center; align-items: center;">
            <div class="col-12 col-md-6 ">
                <h3 style="font-size:18px; color:black; padding-top:20px; font-family:sans-serif;">
                    Experiencia de usuario personalizable
                </h3>
                <p style="font-size:14px; color:#716966; font-family:sans-serif; margin-top: 25px;">
                    Al igual que el experto en virtualización de escritorios Brian Madden dijo "bajo ancho de banda, buena experiencia de usuario, bajo consumo de CPU, elija dos", en la tecnología de escritorio remoto, los usuarios no pueden tener su pastel y comérselo. Con otras soluciones, a los usuarios a menudo no les queda otra opción que elegir por única vez entre la suavidad, la calidad de visualización y el número de usuarios de video simultáneos. Si bien en realidad, el requisito de los usuarios para la experiencia del usuario varía en el tiempo y las circunstancias.
                    <br>
                    <br>
                    La tecnología DDP de vCloudPoint permite a los administradores personalizar la experiencia del usuario para cumplir los requisitos en diferentes escenarios. El administrador puede usar la configuración "óptima" predeterminada para brindar la experiencia informática más rica a los usuarios, y cambiar a la configuración "balanceada" para reducir el consumo de recursos del host mientras permite una reproducción de video HD sin inconvenientes o la configuración "económica" para maximizar el número de usuarios que trabajan Los administradores pueden seguir detalladamente para configurar cada usuario de cliente cero de vCloudPoint, como USB, audio, resolución, video, etc. Al transferir solo píxeles dinámicos, la tecnología DDP de vCloudPoint puede reducir efectivamente el consumo de recursos de la CPU y el ancho de banda de red mientras aún está ofreciendo la mejor calidad de visualización al 95% -96% como la de las PC.
                </p>
            </div>
            <div style="margin-top:50px;" class="col-12 col-md-6 tercer-video">
                <iframe class="tercer-video" src="https://www.youtube.com/embed/O7L1tnGL0h0" width="100%" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
            </div>
        </div>
    </div>

    <div class="container" style="padding-top: 10px; padding-bottom: 10px;">
        <div class="row" style="display: flex; justify-content: center; align-items: center;">
            <div class="col-12 col-md-6 sexto-video">
                <iframe class="sexto-video" src="https://www.youtube.com/embed/SOEBqkgh0CI" width="100%" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
            </div>
            <div style="margin-bottom:15x;" class="col-12 col-md-6">
                <h3 style="font-size:18px; color:black; font-family:sans-serif;">Herramientas utiles</h3>
                <p style="font-size:14px; color:#716966; font-family:sans-serif; margin-top: 25px;">
                    vMatrix Management Center proporciona a los administradores de TI una interfaz fácil de usar con una mayor capacidad de administración. Además de la administración general de usuarios y escritorios, los administradores de TI pueden transmitir su escritorio a los usuarios, comunicarse con ellos a través de la herramienta de chat interna, proporcionar soporte a través de monitoreo y control sobre el escritorio.
                </p>
            </div>
        </div>
    </div>

    <div class="container" style="padding-top: 10px; padding-bottom: 10px;">
        <div class="row" style="display: flex; justify-content: center; align-items: center;">
            <div class="col-12 col-md-6">
                <h3 style="font-size:18px; color:black; padding-top:20px; font-family:sans-serif;">
                    Facil de configurar y usar
                </h3>
                <p style="font-size:14px; color:#716966; font-family:sans-serif; margin-top: 25px;">
                    Hacemos un gran esfuerzo para asegurarnos de que trabajar con los clientes zero de vCloudPoint sea una gran diversión para usted, como lo es para nosotros. Le encantará la forma en que se pueden hacer las cosas con solo unos pocos clics. Tratamos de hacer todo de manera tan intuitiva como solo puede ser. Fácil instalación del software, configuración cero y dispositivos plug-and-play: todo para facilitar su trabajo.
                    <br>
                    Solo existe el software del conjunto de servidores para ejecutar en la computadora host para configurar el sistema. Todo el proceso de instalación es solo unos pocos pasos para seguir y finalizar más la creación del usuario. Ya se trate de un despliegue de 100 o 1000 asientos, se puede completar en solo unos minutos. El cliente cero de vCloudPoint proporciona una experiencia de trabajo práctica para los usuarios. Su interfaz de inicio de sesión permite el descubrimiento y el inventario de hosts en LAN, y el inicio de sesión automático. Solo un clic, están listos para trabajar.
                </p>
            </div>
            <div style="margin-top:80px;" class="col-12 col-md-6 septimo-video">
                <iframe class="septimo-video" src="https://www.youtube.com/embed/iPhoG2U_Plg" width="100%" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
            </div>
        </div>
    </div>


<div class="row" style="background-color:#333333;">
	<div class="container">
		<div class="row" style="margin-right:15px; margin-left:15px;">
			<div class="col-md-3">
				<img class="img-fluid  pt-3 pb-3" src="img/logo vcloudpoint1.png ">
				<p class="text-justify pt-2 footer-descripcion">
					Las tecnologías vCloudPoint hacen que la informática de escritorio sea extremadamente simple y económica. Debido a la excelente
					flexibilidad y simplicidad de sus cliente cero, puede implementar, usar y administrar fácilmente escritorios para docenas
					a cientos de usuarios a una fracción de los costos.
				</p>
			</div>
		
			<div class="col-md-3">
				<p style="color:white;  font-size:20px;" class="pt-3 pb-3 text-center">Contacto</p>
				<a>
					<p class="text-center" style="font-size:13px;"><i class="far fa-envelope"></i>  ventas@vcloudpoint.com.mx</p>
				</a>
				<hr style="background-color:white;">
				<p class="text-center" style="font-size:14px;"><i class="fas fa-phone-volume"></i> 01 (55) 5615-2916</p>
			</div>

			<div class="col-md-3">
				<p style="color:white; font-family: sans-serif; font-size:20px;" class="pt-3 pb-3 text-center">Noticias recientes</p>
				<a href="18-abril.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>
						Aviso: Se solucionó el problema con la actualización del sistema de
						Windows que causaba el inicio de sesión de varios usuarios no funcionara.
					</p>
				</a>
				<p style="font-size:10px;">18 de Abril del 2019</p>
				<hr class="footer2">
				<a href="18-marzo.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>VCLOUDPOINT China fue otorgado como miembro de CEEIA y SZEEIA.</p>
				</a>
				<p style="font-size:10px;">18 de Marzo del 2019</p>
			</div>

			<div class="col-md-3">
				<br>
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:40px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="http://www.facebook.com/vCloudPointMX/"> <i class="fab fa-facebook-f"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://youtu.be/cmTRDUURWTc"><i class="fab fa-youtube"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.linkedin.com/in/vcloudpoint-mexico-86a194168/"><i class="fab fa-linkedin-in"></i></a>
					</div>
				</div>
				
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:50px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://twitter.com/vcloudmx?lang=es"><i class="fab fa-twitter"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.instagram.com/vcloudpointmx/"><i class="fab fa-instagram"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--barra-progreso-->
<script type="text/javascript">
	function animar (){
		document.getElementById("barra1").classList.toggle ("final1");
		document.getElementById("barra2").classList.toggle ("final2");
		document.getElementById("barra3").classList.toggle ("final3");

	}
	window.onload = function (){
	  animar();

	}
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?51BGgA3dFcKcI1mPpd4Un7VFblV9TgM1";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


<footer style="background-color:#262626; height: auto;">
	<div class="container">
		<div class="row">
			<p style="color:#c5c5c5; font-size:14px;" class="text-center copyright w-100">vCloudPoint 2019. Todos los derechos reservados</p>
		</div> <!--.row-->
	</div><!--.container-->
</footer>









    <script src="js/jquery.slim.js"></script>
	<script src="js/app.js"></script>
    <script src="js/menu-activo.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/menu-hover.js"></script>
    <script src="js/validar-numero.js"></script>
  </body>
</html>
